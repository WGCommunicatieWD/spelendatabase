<?php

use App\Http\Requests\ManageUserRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController')->name('home');
Route::redirect('/home', '/');

Route::get('spelen/zoek', [\App\Http\Controllers\SpelController::class, 'search'])
    ->name('spelen.zoek');
Route::get('spelen/{spel}/print', [\App\Http\Controllers\SpelController::class, 'print'])
    ->name('spelen.print');
Route::patch('spelen/{spel}/approve', [\App\Http\Controllers\SpelController::class, 'approve'])
    ->name('spelen.approve');
Route::patch('spelen/{spel}/deny', [\App\Http\Controllers\SpelController::class, 'deny'])
    ->name('spelen.deny');
Route::post('spelen/{spel}/love', [\App\Http\Controllers\SpelController::class, 'love'])
    ->name('spelen.love');
Route::get('spelen/export/{spel}', [\App\Http\Controllers\WordCreatorController::class, 'create'])
    ->name('spelen.export');
Route::get('spelen/{spel}/attachments', [\App\Http\Controllers\AttachmentController::class, 'show'])->name('spelen.attachments');
Route::post('spelen/{spel}/attachments', [\App\Http\Controllers\AttachmentController::class, 'store']);
Route::get('spelen/{spel}/attachments/{attachment}', [\App\Http\Controllers\AttachmentController::class, 'download'])->name('spelen.attachments.download');
Route::delete('spelen/{spel}/attachments/{attachment}', [\App\Http\Controllers\AttachmentController::class, 'destroy'])->name('spelen.attachments.delete');
Route::get('spelen/{spel}/addtogroup', [\App\Http\Controllers\SpelController::class, 'selectGroup'])->name('spelen.addtogroup');
Route::post('spelen/{spel}/addtogroup', [\App\Http\Controllers\SpelController::class, 'addToGroup']);
Route::post('spelen/{spel}/copy', [\App\Http\Controllers\SpelController::class, 'copy'])->name('spelen.copy');
Route::post('spelen/{spel}/clear-flags', [\App\Http\Controllers\SpelController::class, 'clearFlags'])->name('spelen.clear-flags');
Route::resource('spelen', 'SpelController')
    ->parameters(['spelen' => 'spel']);
Route::apiResource('spelen.comments', 'CommentController')
    ->parameters(['spelen' => 'spel']);

Route::resource('verbonden', 'VerbondController')
    ->parameters(['verbonden' => 'verbond']) // Laravel doesn't understand Dutch
    ->only('index', 'show');

Route::resource('categories', 'CategorieController')->only('show', 'store');

Route::post('terrein', [\App\Http\Controllers\TerreinController::class, 'store'])
    ->name('terrein.store')
    ->middleware('verified');
Route::get('terrein/{terrein}', [\App\Http\Controllers\TerreinController::class, 'terrein'])->name('terrein');

Route::patch('users/{user}/promote', [\App\Http\Controllers\UserController::class, 'promote'])
    ->name('users.promote');
Route::patch('users/{user}/demote', [\App\Http\Controllers\UserController::class, 'demote'])
    ->name('users.demote');
Route::patch('users/{user}/verify', [\App\Http\Controllers\UserController::class, 'verify'])
    ->name('users.verify');
Route::patch('users/{user}/unverify', [\App\Http\Controllers\UserController::class, 'unverify'])
    ->name('users.unverify');
Route::resource('users', 'UserController');

Route::get('admin', 'AdminController@index')
    ->name('admin');

Route::get('/groups/{group}/member', [\App\Http\Controllers\GroupController::class, 'newmember'])->name('groups.newmember');
Route::post('/groups/{group}/member', [\App\Http\Controllers\GroupController::class, 'addmember']);
Route::delete('/groups/{group}/member/{user}', [\App\Http\Controllers\GroupController::class, 'removemember'])->name('groups.member');
Route::delete('/groups/{group}/game/{spel}', [\App\Http\Controllers\GroupController::class, 'removegame'])->name('groups.game');
Route::get('/groups/{group}/game/{spel}', [\App\Http\Controllers\GroupController::class, 'editcomment'])->name('groups.editcomment');
Route::put('/groups/{group}/game/{spel}', [\App\Http\Controllers\GroupController::class, 'updatecomment'])->name('groups.updatecomment');
Route::resource('groups', 'GroupController');

Route::get('/migrate', [\App\Http\Controllers\MigrationController::class, 'index']);

Auth::routes(['verify' => true]);
