<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerreinen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terreinen', function (Blueprint $table) {
            $table->increments('id');
			$table->string('naam', 100)->default('Elk terrein');
        });

        DB::table('terreinen')->insert([
            'naam' => 'Elk Terrein'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terreinen');
    }
}
