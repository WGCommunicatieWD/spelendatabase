<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;

class MoveToPermissions extends Migration
{
    public function up()
    {
        Artisan::call('db:seed', ['--class' => 'PermissionsAndRolesSeeder']);

        foreach (User::where('isMod')->get() as $user) {
            $user->assignRole('moderator');
        };

        foreach (User::where('isAdmin', true)->get() as $user) {
            $user->assignRole('admin');
        };

        foreach (User::where('isVerified')->get() as $user) {
            $user->assignRole('verified');
        };

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('isVerified');
            $table->dropColumn('isAdmin');
            $table->dropColumn('isMod');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('isVerified')->default(false);
            $table->boolean('isAdmin')->default(false);
            $table->boolean('isMod')->default(false);
        });

        foreach (Role::findByName('verified')->users as $user) {
            $user->isVerified = true;
        };

        foreach (Role::findByName('admin')->users as $user) {
            $user->isAdmin = true;
        };

        Role::findByName('moderator')->users->forEach(function ($user) {
            $user->isMod = true;
        });
    }
}
