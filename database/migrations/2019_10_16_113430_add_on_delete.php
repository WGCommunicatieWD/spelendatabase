<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('favourites', function (Blueprint $table) {
            $table->dropForeign(['spelId']);
            $table->dropForeign(['userId']);
            $table->foreign('userId')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('spelId')->references('id')->on('spelen')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('favourites', function (Blueprint $table) {
            $table->dropForeign(['spelId']);
            $table->dropForeign(['userId']);
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('spelId')->references('id')->on('spelen');
        });
    }
}
