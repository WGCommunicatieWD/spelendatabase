<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpelen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spelen', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->string('maker', 100)->default('Anoniempje');
			$table->tinyInteger('minLeeftijd')->unsigned()->default(0);
			$table->tinyInteger('maxLeeftijd')->unsigned()->default(99);
			$table->string('titel', 200)->default('Naamloos spel');
			$table->integer('categorieId')->unsigned()->nullable();
			$table->foreign('categorieId')->references('id')->on('categorieen');
			$table->text('uitleg');
			$table->integer('deelnemers')->unsigned();
			$table->smallInteger('duurMinuten')->unsigned()->default(90);
			$table->integer('terreinId')->unsigned()->nullable();
			$table->foreign('terreinId')->references('id')->on('terreinen');
			$table->text('materiaal')->nullable();
			$table->tinyInteger('monis')->unsigned()->default(0);
			$table->text('taakverdeling')->nullable();
			$table->text('inkleding')->nullable();
			$table->boolean('gecontroleerd')->default(false);
			$table->boolean('zichtbaar')->default(false);
			$table->bigInteger('views')->unsigned()->default(0);
			$table->integer('score')->default(0);
			$table->integer('aantalStemmen')->unsigned()->default(0);
			$table->text('inclusie')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spelen');
    }
}
