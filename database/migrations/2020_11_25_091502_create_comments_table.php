<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();

            $table->unsignedInteger('game_id');
            $table->foreign('game_id')
                ->references('id')
                ->on('spelen')
                ->cascadeOnDelete();

            $table->string('content');
        });
    }

    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
