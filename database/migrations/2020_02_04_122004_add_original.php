<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOriginal extends Migration
{
    public function up()
    {
        Schema::table('spelen', function (Blueprint $table) {
            $table->unsignedInteger('original_id')->nullable();
            $table->foreign('original_id')
                ->references('id')
                ->on('spelen')
                ->onDelete('SET NULL');
        });
    }

    public function down()
    {
        Schema::table('spelen', function (Blueprint $table) {
            $table->dropForeign(['original_id']);
            $table->dropColumn('original_id');
        });
    }
}
