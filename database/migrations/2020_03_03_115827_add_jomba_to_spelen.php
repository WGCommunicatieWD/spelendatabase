<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJombaToSpelen extends Migration
{
    public function up()
    {
        Schema::table('spelen', function (Blueprint $table) {
            $table->boolean('is_jomba')->default(false);
        });
    }

    public function down()
    {
        Schema::table('spelen', function (Blueprint $table) {
            $table->dropColumn('is_jomba');
        });
    }
}
