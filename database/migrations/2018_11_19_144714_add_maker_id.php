<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMakerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spelen', function (Blueprint $table) {
            $table->integer('maker')->unsigned()->nullable();
            $table->foreign('maker')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spelen', function (Blueprint $table) {
            $table->dropForeign('spelen_maker_foreign');
            $table->dropColumn('maker');
        });
    }
}
