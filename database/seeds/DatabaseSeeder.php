<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(VerbondSeeder::class);
        $this->call(PermissionsAndRolesSeeder::class);
    }
}
