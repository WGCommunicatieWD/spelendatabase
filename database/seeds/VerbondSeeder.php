<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VerbondSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'name' => 'Waas & Dender'],
            ['id' => 2, 'name' => 'Antwerpen'],
            ['id' => 3, 'name' => 'Brugge'],
            ['id' => 4, 'name' => 'Leuven'],
            ['id' => 5, 'name' => 'Limburg'],
            ['id' => 6, 'name' => 'Oostende'],
            ['id' => 7, 'name' => 'Roeselare-Tielt'],
            ['id' => 8, 'name' => 'Regio Mechelen-Turnhout'],
            ['id' => 9, 'name' => 'Sint-Michielsbond'],
            ['id' => 10, 'name' => 'West-Vlaanderen'],
            ['id' => 11, 'name' => 'MVL - Aalst'],
            ['id' => 12, 'name' => 'MVL - Deinze'],
            ['id' => 13, 'name' => 'MVL - Gent'],
            ['id' => 14, 'name' => 'MVL - Meetjesland'],
            ['id' => 15, 'name' => 'MVL - Vlaamse Ardennen'],
        ];
        foreach ($data as $row) {
            DB::statement('INSERT INTO verbonds (`id`, `name`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `name` = ?',
                          [$row['id'], $row['name'], $row['name']]);
        }
    }
}
