<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsAndRolesSeeder extends Seeder
{
    private function ensurePermission(string $name)
    {
        if (! Permission::where('name', $name)->exists()) {
            $perm = Permission::create(['name' => $name]);
        }
    }

    private function ensureRole(string $name, callable $callback)
    {
        if (! Role::where('name', $name)->exists()) {
            $role = Role::create(['name' => $name]);
            $callback($role);
        }
    }

    public function run()
    {
        $this->ensurePermission('access admin');
        $this->ensurePermission('approve games');
        $this->ensurePermission('force-delete games');
        $this->ensurePermission('edit any game');
        $this->ensurePermission('bypass approval');
        $this->ensurePermission('manage users');
        $this->ensurePermission('manage groups');
        $this->ensurePermission('moderate comments');

        $this->ensureRole('admin', function($role) {
            $role->syncPermissions('access admin', 'approve games', 'force-delete games', 'manage users',
                'edit any game', 'manage groups', 'moderate comments');
        });
        $this->ensureRole('moderator', function($role) {
            $role->syncPermissions('access admin', 'approve games', 'moderate comments');
        });
        $this->ensureRole('verified', function($role) {
            $role->syncPermissions('bypass approval');
        });
        $this->ensureRole('jedi', function($role) {
            $role->syncPermissions('manage users', 'bypass approval');
        });
    }
}
