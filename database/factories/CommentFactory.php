<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->text,
    ];
});

$factory->state(Comment::class, 'flag', [
    'flag' => true,
]);

$factory->state(Comment::class, 'flag_removed', [
    'flag' => true,
    'flag_removed_at' => now(),
]);

$factory->state(Comment::class, 'restricted_to_author', [
    'restricted_to_author' => true,
]);
