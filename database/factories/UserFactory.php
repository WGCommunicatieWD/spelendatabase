<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => Carbon::yesterday()->toDateTimeString(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'verbond_id' => \App\Verbond::all()->random()->id,
    ];
});

$factory->afterMakingState(App\User::class, 'moderator', function ($user, $faker) {
    $user->assignRole('moderator');
});

$factory->afterMakingState(App\User::class, 'admin', function ($user, $faker) {
    $user->assignRole('admin');
});

$factory->afterMakingState(App\User::class, 'verified', function ($user, $faker) {
    $user->assignRole('verified');
});
