<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Group;
use Faker\Generator as Faker;

$factory->define(Group::class, function (Faker $faker) {
    return [
        'name' => $faker->text(50),
        'description' => $faker->paragraph(),
    ];
});

$factory->state(Group::class, 'public', [
    'public' => true,
]);

$factory->state(Group::class, 'private', [
    'public' => false,
]);
