<?php

use Faker\Generator as Faker;

$factory->define(App\Terrein::class, function (Faker $faker) {
    return [
        'naam' => $faker->bs(),
    ];
});
