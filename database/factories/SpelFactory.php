<?php

use Faker\Generator as Faker;

$factory->define(App\Spel::class, function (Faker $faker) {
    return [
        'titel' => $faker->catchPhrase(),
        'uitleg' => $faker->realText(),
        'minLeeftijd' => 0,
        'maxLeeftijd' => 90,
        'deelnemers' => $faker->numberBetween(0, 100),
        'monis' => $faker->numberBetween(0, 10),
    ];
});

$factory->state(\App\Spel::class, 'approved', [
    'gecontroleerd' => true,
    'zichtbaar' => true,
]);

$factory->state(\App\Spel::class, 'denied', [
    'gecontroleerd' => true,
    'zichtbaar' => false,
]);

$factory->state(\App\Spel::class, 'draft', [
    'isDraft' => true,
]);

$factory->state(\App\Spel::class, 'visible-not-checked', [
    'gecontroleerd' => false,
    'zichtbaar' => true,
]);
