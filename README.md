# Spelendatabase
Dit is een nieuwe versie van de Kazou Waas en Dender spelendatabase.
Deze rewrite heeft als belangrijkste doel te werken, altijd en op alle apparaten.

## Installatie
Stap 0: Vereisten
Voor php zijn dit de vereisten:
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

Ook is een database nodig.

Stap 1: Zet de bestanden op de server. Kies een locatie op de server om alles naar te kopiëren, zorg dat php zelf hier in kan schrijven.
De map `public` hoort de webroot te zijn.

Stap 2: Composer. Als Composer niet geïnstalleerd is, kan je het gewoon downloaden naar de server.
Voer vervolgens dit uit: `composer install` om alle dependencies te installeren.
Dit kan eventueel op een andere computer gebeuren, waarna de `vendor` map gekopieerd moet worden.

Stap 3: `.env`. Het `.env`-bestand moet worden ingevuld, dit is makkelijkst door het `.env.example` bestand te hernoemen en de waarden aan te passen.
Let vooral op de `debug=true` sleutel, voor productie servers moet dit zeker op `false` staan. Vul hier ook database- en mailgegevens in.
Voer vervolgens `php artisan key:generate` uit om een encryptiesleutel te genereren. Deze wordt opgeslagen in `.env`.

Stap 4: Database. Met het commando `php artisan migrate` wordt de hele database voorbereid.

Stap 5: Admin. Administrator-gebruikers kunnen worden aangemaakt met het commando `php artisan newuser:admin <username> <email>`.
Dit vraagt om een paswoord.

De spelendatabase is nu klaar voor gebruik!
