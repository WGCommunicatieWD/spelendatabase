<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    public function testIndexNotLoggedIn()
    {
        $response = $this->get(route('admin'));

        $response->assertStatus(403);
    }

    public function testIndexNotAuthorised()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)
                  ->get(route('admin'));

        $response->assertStatus(403);
    }

    public function testIndexAsMod()
    {
        $categories = factory(\App\Categorie::class, 5)->create();
        $terrains = factory(\App\Terrein::class, 5)->create();
        $gamesToCheck = factory(\App\Spel::class, 5)->create();
        $gamesApproved = factory(\App\Spel::class, 5)->states('approved')->create();
        $gamesDenied = factory(\App\Spel::class, 5)->states('denied')->create();
        $user = factory(\App\User::class)
              ->states('moderator')
              ->create();
        $response = $this->actingAs($user)
                  ->get(route('admin'));

        $response->assertStatus(200);
        for ($i = 0; $i < 5; $i++) {
            $response->assertSee($categories[$i]->naam);
            $response->assertSee($terrains[$i]->naam);
            $response->assertSee($gamesToCheck[$i]->titel);
            $response->assertDontSee($gamesApproved[$i]->titel);
            $response->assertSee($gamesDenied[$i]->titel);
        }
    }

    public function testIndexAsAdmin()
    {
        $categories = factory(\App\Categorie::class, 5)->create();
        $terrains = factory(\App\Terrein::class, 5)->create();
        $gamesToCheck = factory(\App\Spel::class, 5)->create();
        $gamesApproved = factory(\App\Spel::class, 5)->states('approved')->create();
        $gamesDenied = factory(\App\Spel::class, 5)->states('denied')->create();
        $user = factory(\App\User::class)
              ->states('admin')
              ->create();
        $response = $this->actingAs($user)
                  ->get(route('admin'));

        $response->assertStatus(200);
        for ($i = 0; $i < 5; $i++) {
            $response->assertSee($categories[$i]->naam);
            $response->assertSee($terrains[$i]->naam);
            $response->assertSee($gamesToCheck[$i]->titel);
            $response->assertDontSee($gamesApproved[$i]->titel);
            $response->assertSee($gamesDenied[$i]->titel);
        }
    }
}
