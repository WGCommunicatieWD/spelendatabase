<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VerbondTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    public function testIndex()
    {
        $response = $this->get(route('verbonden.index'));

        $response->assertStatus(200);
        foreach (\App\Verbond::all() as $verbond) {
            $response->assertSee($verbond->name);
        }
    }

    public function testVerbond()
    {
        $users = array();

        // Generate users
        foreach (\App\Verbond::all() as $verbond)
        {
            $users[] = factory(\App\User::class)->create([
                'verbond_id' => $verbond->id
            ]);
        }

        // Do the tests
        foreach (\App\Verbond::all() as $verbond)
        {
            $response = $this->get(route('verbonden.show', ['verbond' => $verbond]));

            foreach ($users as $user) {
                if ($user->verbond_id === $verbond->id) {
                    $response->assertSee(e($user->name));
                } else {
                    $response->assertDontSee(e($user->name));
                }
            }
        }
    }
}
