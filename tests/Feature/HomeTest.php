<?php

namespace Tests\Feature;

use App\Spel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    public function testHomeNotLoggedIn()
    {
        $categories = factory(\App\Categorie::class, 5)->create();
        $terrains = factory(\App\Terrein::class, 5)->create();
        $games = factory(\App\Spel::class, 5)->states('approved')->create();

        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee(__('Login'));
        $response->assertDontSee('Admin');
        $games->each(function (Spel $game) use ($response) {
            $response->assertSee($game->titel);
        });

        for ($i = 0; $i < 5; $i++) {
            $response->assertSee($categories[$i]->naam);
            $response->assertSee($terrains[$i]->naam);
        }
    }

    public function testHomeLoggedIn()
    {
        $user = factory(\App\User::class)->create();
        $categories = factory(\App\Categorie::class, 5)->create();
        $terrains = factory(\App\Terrein::class, 5)->create();

        $response = $this->actingAs($user)->get('/');

        $response->assertStatus(200);
        $response->assertSee(__('Logout'));
        $response->assertDontSee('Admin');

        for ($i = 0; $i < 5; $i++) {
            $response->assertSee($categories[$i]->naam);
            $response->assertSee($terrains[$i]->naam);
        }
    }

    public function testHomeAsMod()
    {
        $user = factory(\App\User::class)->states('moderator')->create();
        $categories = factory(\App\Categorie::class, 5)->create();
        $terrains = factory(\App\Terrein::class, 5)->create();

        $response = $this->actingAs($user)->get('/');

        $response->assertStatus(200);
        $response->assertSee(__('Logout'));
        $response->assertSee('Admin');

        for ($i = 0; $i < 5; $i++) {
            $response->assertSee($categories[$i]->naam);
            $response->assertSee($terrains[$i]->naam);
        }
    }

    public function testHomeAsAdmin()
    {
        $user = factory(\App\User::class)->states('admin')->create();
        $categories = factory(\App\Categorie::class, 5)->create();
        $terrains = factory(\App\Terrein::class, 5)->create();

        $response = $this->actingAs($user)->get('/');

        $response->assertStatus(200);
        $response->assertSee(__('Logout'));
        $response->assertSee('Admin');

        for ($i = 0; $i < 5; $i++) {
            $response->assertSee($categories[$i]->naam);
            $response->assertSee($terrains[$i]->naam);
        }
    }
}
