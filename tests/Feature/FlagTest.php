<?php

namespace Tests\Feature;

use App\Comment;
use App\Notifications\Flagged;
use App\Notifications\FlagReviewed;
use App\Spel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class FlagTest extends TestCase
{
    use RefreshDatabase;

    public function testFlagStatusNoComments()
    {
        $spel = factory(Spel::class)->states('approved')->create();

        $this->assertFalse($spel->flagged);
    }

    public function testFlagStatusNormalComment()
    {
        $spel = factory(Spel::class)->states('approved')->create();
        $user = factory(User::class)->states('verified')->create();

        $comment = factory(Comment::class)->create([
            'user_id' => $user->id,
            'game_id' => $spel->id,
        ]);

        $this->assertFalse($spel->flagged);
    }

    public function testFlagStatusAuthorComment()
    {
        $spel = factory(Spel::class)->states('approved')->create();
        $user = factory(User::class)->states('verified')->create();

        $comment = factory(Comment::class)->states('restricted_to_author')
            ->create([
                'user_id' => $user->id,
                'game_id' => $spel->id,
            ]);

        $this->assertFalse($spel->flagged);
    }

    public function testFlagStatusFlagReviewed()
    {
        $spel = factory(Spel::class)->states('approved')->create();
        $user = factory(User::class)->states('verified')->create();

        $comment = factory(Comment::class)->states('flag_removed')
            ->create([
                'user_id' => $user->id,
                'game_id' => $spel->id,
            ]);

        $this->assertFalse($spel->flagged);
    }

    public function testFlagStatusFlagged()
    {
        $spel = factory(Spel::class)->states('approved')->create();
        $user = factory(User::class)->states('verified')->create();

        $comment = factory(Comment::class)->states('flag')
            ->create([
                'user_id' => $user->id,
                'game_id' => $spel->id,
            ]);

        $this->assertTrue($spel->flagged);
    }

    public function testFlaggedEmailSent()
    {
        $creator = factory(User::class)->states('verified')->create();
        $mods = factory(User::class, 5)->states('moderator')->create();
        $user = factory(User::class)->states('verified')->create();
        $spel = factory(Spel::class)->states('approved')->create([
            'maker' => $creator->id,
        ]);

        $to_be_notified = $mods->collect()->push($creator)->all();

        Notification::fake();
        Notification::assertNothingSent();

        $this->actingAs($user)->post(route('spelen.comments.store', $spel), [
            'content' => 'abc',
            'restricted_to_author' => 0,
            'flag' => 0,
        ]);

        // We shouldn't get a notification from this
        Notification::assertNotSentTo($to_be_notified, Flagged::class);

        $this->actingAs($user)->post(route('spelen.comments.store', $spel), [
            'content' => 'abc',
            'restricted_to_author' => 1,
            'flag' => 0,
        ]);

        // We shouldn't get a notification from this
        Notification::assertNotSentTo($to_be_notified, Flagged::class);


        $this->actingAs($user)->post(route('spelen.comments.store', $spel), [
            'content' => 'abc',
            'restricted_to_author' => 0,
            'flag' => 1,
        ]);

        Notification::assertSentTo($to_be_notified, Flagged::class,
            function (Flagged $notification, $channels) use ($spel) {
                return $notification->spel->id === $spel->id;
            });

        $this->actingAs($mods[0])->post(route('spelen.clear-flags', $spel));

        Notification::assertSentTo([$creator, $user], FlagReviewed::class);
    }
}
