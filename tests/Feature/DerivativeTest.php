<?php

namespace Tests\Feature;

use App\Spel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DerivativeTest extends TestCase
{
    public function testPublicDerivative() {
        $original = factory(Spel::class)
                  ->states('approved')->create();
        $derivative = factory(Spel::class)
                    ->states('approved')
                    ->create([
                        'original_id' => $original->id,
                    ]);
        $response = $this->get(route('spelen.show', $original));
        $response->assertStatus(200);
        $response->assertSee('Afgeleide spelen');
        $response->assertSee($derivative->titel);
        $response->assertDontSee('Gebaseerd op');
    }

    public function testPublicOriginal() {
        $original = factory(Spel::class)
                  ->states('approved')->create();
        $derivative = factory(Spel::class)
                    ->states('approved')
                    ->create([
                        'original_id' => $original->id,
                    ]);

        $response = $this->get(route('spelen.show', $derivative));
        $response->assertStatus(200);
        $response->assertSee('Gebaseerd op');
        $response->assertSee($original->title);
    }

    public function testCanSeeOwnDerivatives() {
        $user = factory(User::class)->states('verified')->create();
        $original = factory(Spel::class)
                  ->states('approved')->create();
        $derivative = factory(Spel::class)
                    ->create([
                        'original_id' => $original->id,
                        'maker' => $user->id,
                    ]);

        $response = $this->actingAs($user)->get(route('spelen.show', $original));
        $response->assertStatus(200);
        $response->assertSee("Afgeleide spelen");
        $response->assertSee($derivative->titel);
    }

    public function testLinkVisibility()
    {
        $user = factory(User::class)->states('verified')->create();
        $originals = [
            factory(Spel::class)->states('approved')->create(),
            factory(Spel::class)->states('denied')->create(),
            factory(Spel::class)->states('draft')->create(),
            factory(Spel::class)->states('visible-not-checked')->create(),
            factory(Spel::class)->create(),
            factory(Spel::class)->states('approved')->create(['maker' => $user->id]),
            factory(Spel::class)->states('denied')->create(['maker' => $user->id]),
            factory(Spel::class)->states('draft')->create(['maker' => $user->id]),
            factory(Spel::class)->states('visible-not-checked')->create(['maker' => $user->id]),
            factory(Spel::class)->create(['maker' => $user->id]),
        ];
        foreach ($originals as $original) {
            $spel = factory(Spel::class)
                ->states('approved')
                ->create([
                    'original_id' => $original->id,
                ]);

            $response = $this->actingAs($user)->get(route('spelen.show', $spel));

            if ($user->can('view', $spel)) {
                $response->assertSee($original->title);
            } else {
                $response->assertDontSee($original->title);
            }
        }
    }

    public function testCantSeeNoOriginalNoDerivatives() {
        $spel = factory(Spel::class)
              ->states('approved')
              ->create();
        $response = $this->get(route('spelen.show', $spel));
        $response->assertStatus(200);
        $response->assertDontSee("Gebaseerd op");
    }

    public function testCopyGame() {
        $user = factory(User::class)->states('verified')->create();
        $original = factory(Spel::class)
                  ->states('approved')
                  ->create();
        $response = $this->actingAs($user)->followingRedirects()
                  ->post(route('spelen.copy', $original));
        $response->assertStatus(200);
        $response->assertSee($original->titel); // Just a sample of all properties
        $response->assertSee($user->name);
        $response->assertSee('Gebaseerd op');
    }
}
