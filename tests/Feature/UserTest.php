<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    public function testIndex()
    {
        $users = factory(User::class, 5)->create();
        $response = $this->get(route('users.index'));

        // Normal users can't access this
        $response->assertStatus(403);

        $admin = factory(User::class)->states('admin')->create();
        $response = $this->actingAs($admin)
                  ->get(route('users.index'));
        $response->assertStatus(200);
        foreach ($users as $user) {
            $response->assertSee($user->name);
        }
    }

    public function testUser()
    {
        $user = factory(User::class)->create();
        $response = $this->get(route('users.show', ['user' => $user]));
        $response->assertStatus(200);
    }

    public function testUserAuthorised()
    {
        $acting_user = factory(User::class)->states('admin')->create();
        $user = factory(User::class)->create();
        $response = $this
                  ->actingAs($acting_user)
                  ->get(route('users.show', ['user' => $user]));

        $response->assertStatus(200);
        $response->assertSee("Promoveer gebruiker");
    }

    public function testNonExisting()
    {
        $response = $this->get(route('users.show', ['user' => 512]));
        $response->assertStatus(404);
    }

    public function testManageNotAuthorized()
    {
        $acting_user = factory(User::class)->create();
        // Test with mod and normal user
        $users = [factory(User::class)->create(), factory(User::class)
                  ->states('moderator')->create()];

        foreach ($users as $user) {
            // Test promotion
            $response = $this->actingAs($acting_user)
                      ->patch(route('users.promote', ['user' => $user]));
            $response->assertStatus(403);

            // Test demotion
            $response = $this->actingAs($acting_user)
                      ->patch(route('users.demote', ['user' => $user]));
            $response->assertStatus(403);

            // Test verification
            $response = $this->actingAs($acting_user)
                      ->patch(route('users.verify', ['user' => $user]));
            $response->assertStatus(403);

            // Test unverification
            $response = $this->actingAs($acting_user)
                      ->patch(route('users.unverify', ['user' => $user]));
            $response->assertStatus(403);
        }
    }

    public function testAuthorizedCanPromote()
    {
        $acting_user = factory(User::class)->states('admin')->create();
        $user = factory(User::class)->create();

        // Test promotion
        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->patch(route('users.promote', ['user' => $user]));
        $response->assertStatus(200);
        $user = User::find($user->id);
        $this->assertEquals(true, $user->hasRole('moderator'));
    }

    public function testAuthorizedCanDemote()
    {
        $acting_user = factory(User::class)->states('admin')->create();
        $user = factory(User::class)->states('moderator')->create();

        // Test demotion
        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->patch(route('users.demote', ['user' => $user]));
        $response->assertStatus(200);
        $user = User::find($user->id);
        $this->assertEquals(false, $user->hasRole('moderator'));
    }

    public function testAuthorizedCanVerify()
    {
        $acting_user = factory(User::class)->states('admin')->create();
        $user = factory(User::class)->create();
        // Test verification
        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->patch(route('users.verify', ['user' => $user]));
        $response->assertStatus(200);
        $user = User::find($user->id);
        $this->assertEquals(true, $user->hasRole('verified'));
    }

    public function testAuthorizedCanUnverify()
    {
        $acting_user = factory(User::class)->states('admin')->create();
        $user = factory(User::class)->states('verified')->create();

        // Test unverification
        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->patch(route('users.unverify', ['user' => $user]));
        $response->assertStatus(200);
        $user = User::find($user->id);
        $this->assertEquals(false, $user->hasRole('verified'));
    }

    public function testCanSeeOwnGroups()
    {
        $acting_user = factory(User::class)->states('verified')->create();
        $group = factory(\App\Group::class)->states('private')->create();
        $group->members()->attach($acting_user);

        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->get(route('users.show', ['user' => $acting_user]));
        $response->assertStatus(200);
        $response->assertSee($group->name);
    }

    public function testOtherCanSeePublicGroups()
    {
        $acting_user = factory(User::class)->states('verified')->create();
        $user = factory(User::class)->states('verified')->create();
        $group = factory(\App\Group::class)->states('public')->create();
        $group->members()->attach($user);

        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->get(route('users.show', ['user' => $user]));
        $response->assertStatus(200);
        $response->assertSee($group->name);
    }

    public function testOtherCantSeePrivateGroups()
    {
        $acting_user = factory(User::class)->states('verified')->create();
        $user = factory(User::class)->states('verified')->create();
        $group = factory(\App\Group::class)->states('private')->create();
        $group->members()->attach($user);

        $response = $this->actingAs($acting_user)
                  ->followingRedirects()
                  ->get(route('users.show', ['user' => $user]));
        $response->assertStatus(200);
        $response->assertDontSee($group->name);
    }
}
