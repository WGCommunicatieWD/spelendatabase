<?php

namespace Tests\Feature;

use Artisan;
use App\Spel;
use App\Favourite;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestBug48 extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    public function testDeleteFavouritedGame() {
        $spel = factory(\App\Spel::class)->states('approved')->create();
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->post(route('spelen.love', $spel));

        $this->assertDatabaseHas('favourites', [
            'spelId' => $spel->id,
            'userId' => $user->id,
        ]);

        // Normal behaviour
        $response = $this->get(route('users.show', $user));
        $response->assertStatus(200);
        $response->assertSee($spel->titel);
        $this->assertTrue(Favourite::count() == 1);

        // Soft-delete the game
        $spel->delete();
        // Check if it is so
        $this->assertTrue(Spel::count() == 0);
        $this->assertTrue(Spel::withTrashed()->count() == 1);

        // This is where old code would crash, as it would try to load the non-existing game
        // referenced by the favourite
        $response = $this->get(route('users.show', $user));
        $response->assertStatus(200);
        $response->assertDontSee($spel->titel);

        // Check the favourite is soft deleted
        $this->assertTrue(Favourite::count() == 0);
        $this->assertTrue(Favourite::withTrashed()->count() == 1);
    }

    public function testRestoreGameWithFavourite() {
        $spel = factory(\App\Spel::class)->states('approved')->create();
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->post(route('spelen.love', $spel));

        // Trigger the soft-deletion (cascading onto the favourite)
        $spel->delete();
        $this->assertTrue(Favourite::count() == 0);
        $this->assertTrue(Favourite::withTrashed()->count() == 1);

        // Restore
        $spel->restore();
        $this->assertTrue(Favourite::count() == 1);
        $this->assertTrue(Favourite::withTrashed()->count() == 1);

    }
}
