<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TerreinTest extends TestCase
{
    use RefreshDatabase;

    public function testView()
    {
        $terrein = factory(\App\Terrein::class)->create();
        $response = $this->get(route('terrein', ['terrein' => $terrein]));

        $response->assertStatus(200);
        $response->assertSee($terrein->naam);
    }

    public function testNotExisting()
    {
        $response = $this->get(route('terrein', ['terrein' => 512]));

        $response->assertNotFound();
    }

    public function testCreateNotAuthorised()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)
                  ->call('POST', route('terrein.store'), ['naam' => 'bullshit']);
        $response->assertStatus(403);
    }

    public function testCreateAuthorised()
    {
        $user = factory(\App\User::class)->states('moderator')->create();
        $response = $this->followingRedirects()
                  ->actingAs($user)
                  ->call('POST', route('terrein.store'), ['naam' => 'bullshit']);
        // This should redirect to the new category
        $response->assertStatus(200);
        $response->assertSee('bullshit');
    }
}
