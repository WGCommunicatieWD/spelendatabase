<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategorieTest extends TestCase
{
    use RefreshDatabase;

    public function testView()
    {
        $cat = factory(\App\Categorie::class)->create();
        $response = $this->get(route('categories.show', ['category' => $cat]));

        $response->assertStatus(200);
        $response->assertSee($cat->naam);
    }

    public function testNotExisting()
    {
        $response = $this->get(route('categories.show', ['category' => 512]));

        $response->assertNotFound();
    }

    public function testCreateNotAuthorised()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)
                  ->call('POST', route('categories.store'), ['naam' => 'bullshit']);
        $response->assertStatus(403);
    }

    public function testCreateAuthorised()
    {
        $user = factory(\App\User::class)->states('moderator')->create();
        $response = $this->followingRedirects()
                  ->actingAs($user)
                  ->call('POST', route('categories.store'), ['naam' => 'bullshit']);
        // This should redirect to the new category
        $response->assertStatus(200);
        $response->assertSee('bullshit');
    }
}
