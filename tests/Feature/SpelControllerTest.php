<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Notification;

class SpelControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private function refreshSpelen() {
        $this->spelen = factory(\App\Spel::class, 5)
                      ->states('approved')->create();
        $this->spelen = $this->spelen
                      ->concat(factory(\App\Spel::class, 5)->create());
        $this->spelen = $this->spelen
                      ->concat(factory(\App\Spel::class, 5)
                               ->states('denied')->create());
        $this->spelen = $this->spelen->concat(factory(\App\Spel::class, 5)
                                              ->states('approved', 'draft')
                                              ->create());
        $this->spelen = $this->spelen->concat(factory(\App\Spel::class, 5)
                                              ->states('visible-not-checked')
                                              ->create());
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->users = [factory(\App\User::class)->states('moderator')->create(),
                        factory(\App\User::class)->states('admin')->create(),
                        factory(\App\User::class)->states('verified')->create(),
                        factory(\App\User::class)->create(),
        ];
        $this->refreshSpelen();
    }

    // No need to test the actual showing of the games,
    // the repo is unit tested
    public function testIndex()
    {
        $this->get(route('home'))->assertStatus(200);

        foreach ($this->users as $user) {
            $this->actingAs($user)
                 ->get(route('home'))->assertStatus(200);
        }
    }

    public function testSpelNoSpecialRights()
    {
        $users = [factory(\App\User::class)->create(),
                  factory(\App\User::class)->states('verified')->create()
        ];

        foreach ($this->spelen as $spel) {
            $response = $this->get(route('spelen.show', ['spel' => $spel]));
            // The edit button
            $response->assertDontSee('fa-pencil');
            $response->assertDontSee('Verwijder');
            $response->assertDontSee('Keur goed');
            $response->assertDontSee('Keur af');
            $response->assertDontSee('Love');
            if ($spel->visible_by_url) {
                $response->assertStatus(200);
                $response->assertSee($spel->titel);
                $response->assertSessionHas('viewed'.$spel->id);
            } else {
                $response->assertDontSee($spel->titel);
                $response->assertSessionMissing('viewed'.$spel->id);
            }
        }

        foreach ($users as $user) {
            foreach ($this->spelen as $spel) {
                $response = $this->actingAs($user)
                                 ->get(route('spelen.show', ['spel' => $spel]));
                $response->assertDontSee('fa-pencil');
                $response->assertDontSee('Verwijder');
                $response->assertDontSee('Keur goed');
                $response->assertDontSee('Keur af');
                if ($spel->visible_by_url) {
                    $response->assertStatus(200);
                    $response->assertSee($spel->titel);
                    $response->assertSessionHas('viewed'.$spel->id);
                } else if ($user->hasVerifiedEmail()) {
                    $response->assertStatus(403);
                    $response->assertDontSee($spel->titel);
                    $response->assertSessionMissing('viewed'.$spel->id);
                } else {
                    $response->assertStatus(302); // redirect (to verify then login)
                    $response->assertSessionMissing('viewed'.$spel->id);
                }
            }
        }
    }

    public function testSpelOwner() {
        $user = factory(\App\User::class)->create();
        $user->spelen()->save(factory(\App\Spel::class)->make());
        $user->spelen()->save(factory(\App\Spel::class)
                              ->states('draft')->make());

        foreach ($user->spelen as $spel) {
            $response = $this->actingAs($user)
                             ->get(route('spelen.show', ['spel' => $spel]));
            $response->assertStatus(200);
            $response->assertSee('fa-pencil');
            $response->assertSee('Verwijder');
            $response->assertDontSee('Keur goed');
            $response->assertDontSee('Keur af');
            $response->assertDontSee('Love');
            $response->assertSessionMissing('viewed'.$spel->id);
            if ($spel->isDraft)
                $response->assertSee('Klad');
        }
    }

    public function testSpelAdmin()
    {
        $user = factory(\App\User::class)->states('admin')->create();
        foreach ($this->spelen as $spel) {
            $response = $this->actingAs($user)
                             ->get(route('spelen.show', ['spel' => $spel]));

            $response->assertStatus(200);
            if ($user->hasRole('admin'))
                $response->assertSee("Verwijder");
            if ($spel->isDraft)
                $response->assertSee("Klad");

            // This is the edit button
            $response->assertSee("fa-pencil");
            if ($spel->zichtbaar) {
                $response->assertSee("Keur af");
                $response->assertDontSee("Keur goed");
            } else {
                $response->assertSee("Keur goed");
                $response->assertDontSee("Keur af");
            }
        }
    }

    public function testGetCreate()
    {
        $response = $this->get(route('spelen.create'));
        $response->assertStatus(403);

        $users = [factory(\App\User::class)->states('moderator')->create(),
                  factory(\App\User::class)->states('admin')->create(),
                  factory(\App\User::class)->states('verified')->create()];

        foreach ($users as $user) {
            $response = $this->actingAs($user)
                             ->get(route('spelen.create'));
            $response->assertStatus(200);
        }
    }

    public function testPrint() {
        foreach ($this->spelen as $spel) {
            $response = $this->get(route('spelen.print', ['spel' => $spel]));
            if ($spel->visible_by_url) {
                $response->assertStatus(200);
                $response->assertSee($spel->titel);
            } else {
                $response->assertStatus(403);
                $response->assertDontSee($spel->titel);
            }
        }
    }

    public function testViewCount() {
        $spel = factory(\App\Spel::class)->states('approved')->create();
        $user = factory(\App\User::class)->create();

        $response = $this->actingAs($user)->get(route('spelen.show', ['spel' => $spel]));
        $spel->refresh();

        $response->assertStatus(200);
        $response->assertSessionHas('viewed'.$spel->id);
        $this->assertEquals(1, $spel->views);

        $response = $this->actingAs($user)->get(route('spelen.show', ['spel' => $spel]));
        $spel->refresh();

        $this->assertEquals(1, $spel->views);
    }

    public function testApproveAnonymous() {
        foreach ($this->spelen as $spel) {
            $oldCheckStatus = $spel->gecontroleerd;
            $oldVisible = $spel->zichtbaar;
            $response = $this
                      ->call('PATCH', route('spelen.approve', ['spel' => $spel]));

            $response->assertStatus(403);
            $spel->refresh();
            $this->assertEquals($oldCheckStatus, $spel->gecontroleerd);
            $this->assertEquals($oldVisible, $spel->zichtbaar);
        }
    }
    public function testApproveLoggedIn() {
        foreach ($this->users as $user)
            foreach ($this->spelen as $spel) {
                $oldCheckStatus = $spel->gecontroleerd;
                $oldVisible = $spel->zichtbaar;
                $response = $this->actingAs($user)
                                 ->followingRedirects()
                                 ->call('PATCH', route('spelen.approve', $spel));
                if ($user->can('approve games') && !( $spel->isDraft)) {
                    $response->assertStatus(200);
                    $response->assertSee('Keur af');
                    $spel->refresh();
                    $this->assertEquals(true, $spel->zichtbaar);
                    $this->assertEquals(true, $spel->gecontroleerd);
                    $this->refreshSpelen();
                } else {
                    $response->assertStatus(403);
                    $spel->refresh();
                    $this->assertEquals($oldCheckStatus, $spel->gecontroleerd);
                    $this->assertEquals($oldVisible, $spel->zichtbaar);
                }
            }
    }

    public function testOwnerApproveGame() {
        $user = factory(\App\User::class)->create();
        $spel = factory(\App\Spel::class)->create([
            'maker' => $user->id
        ]);

        // Just make sure we get no sending approval mails
        Notification::fake();
        $response = $this->followingRedirects()
                         ->call('PATCH', route('spelen.approve', ['spel' => $spel]));
        $spel->refresh();
        $this->assertEquals(false, $spel->zichtbaar);
        $this->assertEquals(false, $spel->gecontroleerd);
    }

    public function testApproveNotification() {
        $maker = factory(\App\User::class)->create();
        $spel = factory(\App\Spel::class)
              ->create(['maker' => $maker->id]);

        foreach ($this->users as $user) {
            // We need a new NotificationFaker
            // else the previous notification will be counted
            // against the next user
            Notification::fake();
            $response = $this->actingAs($user)
                             ->followingRedirects()
                             ->call('PATCH', route('spelen.approve', ['spel' => $spel]));

            if ($user->hasRole('admin') || $user->hasRole('moderator')) {
                $response->assertStatus(200);
                Notification::assertSentTo(
                    $maker,
                    \App\Notifications\GameApproved::class,
                    function ($notification, $channels) use ($spel) {
                        return $notification->spel->id === $spel->id;
                    });
            } else {
                $response->assertStatus(403);
                Notification::assertNotSentTo(
                    $maker,
                    \App\Notifications\GameApproved::class);
            }
        }
    }

    public function testOwnerDenyGame() {
        $user = factory(\App\User::class)->create();
        $spel = factory(\App\Spel::class)->create([
            'maker' => $user->id
        ]);

        // Just make sure we get no sending deny mails
        Notification::fake();
        $response = $this->followingRedirects()
                         ->call('PATCH', route('spelen.deny', ['spel' => $spel]));
        $spel->refresh();
        $this->assertEquals(false, $spel->zichtbaar);
        $this->assertEquals(false, $spel->gecontroleerd);
    }

    public function testDenyAnonymous() {
        // Test anonymously
        foreach ($this->spelen as $spel) {
            $oldCheckStatus = $spel->gecontroleerd;
            $oldVisible = $spel->zichtbaar;
            $response = $this
                      ->call('PATCH', route('spelen.deny', ['spel' => $spel]));

            $response->assertStatus(403);
            $spel->refresh();
            $this->assertEquals($oldCheckStatus, $spel->gecontroleerd);
            $this->assertEquals($oldVisible, $spel->zichtbaar);
        }
    }

    public function testDenyLoggedIn() {
        // Test logged in
        foreach ($this->users as $user)
            foreach ($this->spelen as $spel) {
                $oldCheckStatus = $spel->gecontroleerd;
                $oldVisible = $spel->zichtbaar;
                $response = $this->actingAs($user)
                                 ->call('PATCH', route('spelen.deny', ['spel' => $spel]));
                if ($user->can('approve games') && (! $spel->isDraft)) {
                    $response->assertRedirect(route('spelen.show', $spel));
                    $spel->refresh();
                    $this->assertEquals(false, $spel->zichtbaar);
                    $this->assertEquals(true, $spel->gecontroleerd);
                    $response = $this->actingAs($user)
                                     ->get(route('spelen.show', $spel));
                    $response->assertStatus(200);
                    $response->assertSee('Keur goed');
                    $this->refreshSpelen();
                } else {
                    $response->assertStatus(403);
                    $spel->refresh();
                    $this->assertEquals($oldCheckStatus, (bool)$spel->gecontroleerd);
                    $this->assertEquals($oldVisible, (bool)$spel->zichtbaar);
                }
            }
    }

    public function testDenyNotification() {
        $maker = factory(\App\User::class)->create();
        $spel = factory(\App\Spel::class)
              ->create(['maker' => $maker->id]);

        foreach ($this->users as $user) {
            // We need a new NotificationFaker
            // else the previous notification will be counted
            // against the next user
            Notification::fake();
            $response = $this->actingAs($user)
                             ->followingRedirects()
                             ->call('PATCH', route('spelen.deny', ['spel' => $spel]));

            if ($user->hasRole('admin') || $user->hasRole('moderator')) {
                $response->assertStatus(200);
                Notification::assertSentTo(
                    $maker,
                    \App\Notifications\GameNotApproved::class,
                    function ($notification, $channels) use ($spel) {
                        return $notification->spel->id === $spel->id;
                    });
            } else {
                $response->assertStatus(403);
                Notification::assertNotSentTo(
                    $maker,
                    \App\Notifications\GameNotApproved::class);
            }
        }
    }

    public function testLoveNotOwner() {
        // Test not logged in
        foreach ($this->spelen as $spel) {
            $oldLoves = $spel->favourites()->count();
            $response = $this
                      ->call('POST', route('spelen.love', ['spel' => $spel]));
            $response->assertStatus(403);
            $spel->refresh();
            $this->assertEquals($oldLoves, $spel->favourites()->count());
        }

        // Test other users
        foreach ($this->users as $user) {
            foreach ($this->spelen as $spel) {
                // Love the game
                $spel->refresh();
                $oldLoves = $spel->favourites()->count();
                $response = $this
                          ->actingAs($user)
                          ->call('POST', route('spelen.love', $spel));
                $user->refresh();

                if ($spel->visible_by_url) {
                    // Test for normal users
                    $response->assertRedirect(route('spelen.show', $spel));
                    $this->assertEquals($oldLoves + 1, $spel->favourites()->count());

                    $response = $this->actingAs($user)->get(route('spelen.show', $spel));
                    $response->assertStatus(200);
                    $response->assertSee('hart');

                    // Unlove now
                    $response = $this->followingRedirects()
                        ->actingAs($user)
                        ->call('POST', route('spelen.love', ['spel' => $spel]));
                    $spel->refresh();
                    $this->assertEquals($oldLoves, $spel->favourites()->count());
                    $response->assertSee('hart-off');
                } else if ($user->can('approve games')) {
                    // People with `approve games` can see more games

                    $response->assertRedirect(route('spelen.show', $spel));
                    $this->assertEquals($oldLoves + 1, $spel->favourites()->count());

                    $response = $this->actingAs($user)->get(route('spelen.show', $spel));
                    $response->assertStatus(200);
                    $response->assertSee('hart');

                    // Unlove now
                    $response = $this->followingRedirects()
                        ->actingAs($user)
                        ->call('POST', route('spelen.love', ['spel' => $spel]));
                    $spel->refresh();
                    $this->assertEquals($oldLoves, $spel->favourites()->count());
                    $response->assertSee('hart-off');
                } else {
                    //var_dump($spel);
                    $response->assertStatus(403);
                    $this->assertEquals($oldLoves, $spel->favourites()->count());
                }
            }
        }
    }

    public function testLoveOwner() {
        foreach ($this->users as $user) {
            // Admins are an exception on Gate rules...
            if ($user->hasRole('admin'))
                continue;

            $user->spelen()->save(factory(\App\Spel::class)->states('approved')->make());
            $spel = $user->spelen()->first();
            $oldLoves = $spel->favourites()->count();

            $response = $this->actingAs($user)
                             ->followingRedirects()
                             ->call('POST', route('spelen.love', ['spel' => $spel]));

            $response->assertStatus(403);
            $spel->refresh();
            $this->assertEquals($oldLoves, $spel->favourites()->count());
        }
    }

    public function testDestroyAnonymous() {
        foreach ($this->spelen as $spel) {
            $response = $this->delete(route('spelen.show', ['spel' => $spel]));
            $response->assertStatus(403);
            $spel->refresh();
            $this->assertEquals(false, $spel->trashed());
        }
    }

    public function testDestroyAsUserNotOwner() {
        foreach ($this->users as $user) {
            foreach ($this->spelen as $spel) {
                $response = $this->followingRedirects()
                                 ->actingAs($user)
                                 ->delete(route('spelen.show', ['spel' => $spel]));
                $spel->refresh();

                if ($user->hasRole('admin')) {
                    $response->assertStatus(200);
                    // We use soft deletes
                    $this->assertEquals(true, $spel->trashed());
                    // Assert it's gone
                    $this->get(route('spelen.show', ['spel' => $spel]))->assertStatus(404);
                    // Restore it
                    $spel->restore();
                } else {
                    $response->assertStatus(403);
                    $this->assertEquals(false, $spel->trashed());
                }
            }
        }
    }

    public function testDestroyAsOwner() {
        $user = factory(\App\User::class)->create();
        $spel = factory(\App\Spel::class)->create([
            'maker' => $user->id
        ]);
        $response = $this->followingRedirects()
                         ->actingAs($user)
                         ->delete(route('spelen.show', ['spel' => $spel]));
        $spel->refresh();

        $response->assertStatus(200);
        // We use soft deletes
        $this->assertEquals(true, $spel->trashed());
        // Assert it's gone
        $this->get(route('spelen.show', ['spel' => $spel]))->assertStatus(404);
    }

    public function testEditAnonymous() {
        foreach ($this->spelen as $spel) {
            $response = $this
                      ->get(route('spelen.edit', ['spel' => $spel]));
            $response->assertStatus(403);
        }
    }

    public function testEditAsUser() {
        foreach ($this->users as $user) {
            foreach ($this->spelen as $spel) {
                $response = $this->actingAs($user)
                                 ->get(route('spelen.edit', ['spel' => $spel]));
                if ($user === $spel->user || $user->hasRole('admin')) {
                    $response->assertStatus(200);
                } else {
                    $response->assertStatus(403);
                }
            }
        }
    }

    public function testSearchParameters() {
        // There's no code differentiating between users,
        // so we do this all anonymously

        // I can't test a valid category in the unit test
        // since there is no DB then.
        $categorie = factory(\App\Categorie::class)->create();
        $terrein = factory(\App\Terrein::class)->create();
        $parameters = ['categorie' => $categorie->id,
                       'terrein' => $terrein->id];
        $response = $this->get(route('spelen.zoek', $parameters));
        $response->assertStatus(200);

        // Bad parameters
        // Are tested by unit test on GameSearchRequest
    }

    public function testSearch() {
        $categorie = factory(\App\Categorie::class)->create();
        $terrein = factory(\App\Terrein::class)->create();
        $otherTerrein = factory(\App\Terrein::class)->create();
        $spelFound = factory(\App\Spel::class)
                   ->states('approved')
                   ->create(['categorieId' => $categorie->id,
                             'terreinId' => $terrein->id,
                             'minLeeftijd' => 0,
                             'maxLeeftijd' => 12]);
        $spelNotApproved = factory(\App\Spel::class)
                         ->create(['categorieId' => $categorie->id,
                                   'terreinId' => $terrein->id,
                                   'minLeeftijd' => 0,
                                   'maxLeeftijd' => 12]);
        $spelNotFound = factory(\App\Spel::class)
                      ->states('approved')->create([
                          'terreinId' => $otherTerrein->id,
                      ]);

        $attributes = ['categorie' => $categorie->id,
                       'terrein' => $terrein->id,
                       'minLeeftijd' => 0,
                       'maxLeeftijd' => 99];
        $response = $this->get(route('spelen.zoek', $attributes));

        $response->assertSessionDoesntHaveErrors();
        $response->assertSee($spelFound->titel);
        $response->assertDontSee($spelNotApproved->titel);
        $response->assertDontSee($spelNotFound->titel);
    }

    public function testSearchVerbond() {
        $verbond = \App\Verbond::all()->random();
        $user = factory(\App\User::class)->create([
            'verbond_id' => $verbond->id,
        ]);
        $spel = factory(\App\Spel::class)
              ->states('approved')
              ->create(['maker' => $user->id]);

        foreach (\App\Verbond::all() as $v) {
            $response = $this->get(route('spelen.zoek', ['verbond_id' => $v->id]));
            $response->assertStatus(200);
            if ($v->id == $verbond->id) {
                $response->assertSee(e($spel->titel));
            } else {
                $response->assertDontSee(e($spel->titel));
            }
        }
    }

    // TODO: update
    // TODO: store
}
