<?php

namespace Tests\Unit;

use Tests\TestCase;
use Validator;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Requests\GameSearchRequest;

class GameSearchRequestTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->rules = (new GameSearchRequest())->rules();
        $this->validator = $this->app['validator'];
    }

    public function testCategorie()
    {
        $this->assertTrue($this->validateField('categorie', null));
        $this->assertFalse($this->validateField('categorie', 'Elke categorie'));
        $this->assertFalse($this->validateField('categorie', -5));
        // If this exists in the database, I'm sorry
        $this->assertFalse($this->validateField('categorie', 12345));
    }

    public function testTerrein()
    {
        $this->assertTrue($this->validateField('terrein', null));
        $this->assertFalse($this->validateField('terrein', 'Elk terrein'));
        $this->assertFalse($this->validateField('terrein', -5));
        // If this exists in the database, I'm sorry
        $this->assertFalse($this->validateField('terrein', 12345));
    }

    public function testMinLeeftijd()
    {
        $this->assertTrue($this->validateField('minLeeftijd', null));
        $this->assertFalse($this->validateField('minLeeftijd', -1));
        $this->assertFalse($this->validateField('minLeeftijd', 'Tien'));
    }

    public function testMaxLeeftijd()
    {
        $this->assertTrue($this->validateField('maxLeeftijd', null));
        $this->assertFalse($this->validateField('maxLeeftijd', -1));
        $this->assertFalse($this->validateField('maxLeeftijd', 'Tien'));
    }

    public function testLeeftijd()
    {
        for ($i = 0; $i < 10; $i++) {
            $attributes = ['minLeeftijd' => 0, 'maxLeeftijd' => $i];
            $validator = Validator::make($attributes, $this->rules);
            $this->assertFalse($validator->fails());
        }
    }

    public function testDuur()
    {
        $this->assertTrue($this->validateField('duur', null));
        $this->assertTrue($this->validateField('duur', 90));
        $this->assertFalse($this->validateField('duur', -1));
        $this->assertFalse($this->validateField('duur', '30 minuten'));
    }

    public function testIsJomba()
    {
        $this->assertTrue($this->validateField('is_jomba', null));
        $this->assertTrue($this->validateField('is_jomba', true));
        $this->assertTrue($this->validateField('is_jomba', false));
        $this->assertTrue($this->validateField('is_jomba', 1));
        $this->assertTrue($this->validateField('is_jomba', 0));
        $this->assertFalse($this->validateField('is_jomba', "null"));
    }

    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }
}
