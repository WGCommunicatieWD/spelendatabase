<?php

namespace Tests\Unit;

use App\Spel;
use PHPUnit\Framework\TestCase;

class SpelTest extends TestCase
{
    public function testDraftsNotPublic(): void
    {
        $spel = new Spel(['isDraft' => true]);
        $spel->zichtbaar = true;
        $spel->gecontroleerd = true;

        $this->assertFalse($spel->public);
        $this->assertTrue($spel->visible_by_url);
    }

    public function testUncheckedNotPublic(): void
    {
        $spel = new Spel(['isDraft' => false]);
        $spel->zichtbaar = true;
        $spel->gecontroleerd = false;

        $this->assertFalse($spel->public);
        $this->assertTrue($spel->visible_by_url);
    }

    public function testRejectedNotPublic(): void
    {
        $spel = new Spel(['isDraft' => false]);
        $spel->zichtbaar = false;
        $spel->gecontroleerd = true;

        $this->assertFalse($spel->public);
        $this->assertFalse($spel->visible_by_url);
    }

    public function testApprovedPublic(): void
    {
        $spel = new Spel(['isDraft' => false]);
        $spel->zichtbaar = true;
        $spel->gecontroleerd = true;

        $this->assertTrue($spel->public);
        $this->assertTrue($spel->visible_by_url);
    }
}
