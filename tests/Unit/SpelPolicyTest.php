<?php

namespace Tests\Unit;

use App\Policies\SpelPolicy;
use App\User;
use App\Spel;
use Tests\TestCase;

class SpelPolicyTest extends TestCase
{
    private $policy = null;

    public function setUp() : void
    {
        parent::setUp();

        if ($this->policy == null)
            $this->policy = new SpelPolicy;
    }

    public function testViewAnyNoUser()
    {
        $this->assertTrue($this->policy->viewAny(null)->allowed());
    }

    public function testViewAnyWithUser()
    {
        $user = factory(\App\User::class)->make();
        $this->assertTrue($this->policy->viewAny($user)->allowed());
    }

    public function testViewUncheckedNoUser()
    {
        // gecontroleerd = null, zichtbaar = null, isDraft = null
        $spel = factory(\App\Spel::class)->make();
        $this->assertTrue($this->policy->view(null, $spel)
                          ->allowed());
    }

    public function testViewUncheckedNormalUser()
    {

        $user = factory(\App\User::class)->make();
        $spel = factory(\App\Spel::class)->make();
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewUncheckedMaker()
    {
        $user = factory(\App\User::class)->make([
            'id' => 5,
        ]);
        $spel = factory(\App\Spel::class)->make([
            'maker' => 5,
        ]);
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewUncheckedMod()
    {
        $user = factory(\App\User::class)->states('moderator')->make();
        $spel = factory(\App\Spel::class)->make();
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    // Drafts are visible by URL (to gather feedback, for example)
    public function testViewDraftNoUser()
    {
        $spel = factory(\App\Spel::class)->states('draft')->make();
        $this->assertTrue($this->policy->view(null, $spel)
                          ->allowed());
    }

    public function testViewApprovedNoUser()
    {
        $spel = factory(\App\Spel::class)->states('approved')->make();
        $this->assertTrue($this->policy->view(null, $spel)
                          ->allowed());
    }

    public function testViewApprovedNormalUser()
    {
        $user = factory(\App\User::class)->make();
        $spel = factory(\App\Spel::class)->states('approved')->make();
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewApprovedMaker()
    {
        $user = factory(\App\User::class)->make([
            'id' => 5,
        ]);
        $spel = factory(\App\Spel::class)->states('approved')
              ->make([
                  'maker' => 5,
              ]);
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewApprovedMod()
    {
        $user = factory(\App\User::class)->states('moderator')->make();
        $spel = factory(\App\Spel::class)->states('approved')->make();
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewDeniedNoUser()
    {
        $spel = factory(\App\Spel::class)->states('denied')->make();
        $this->assertFalse($this->policy->view(null, $spel)
                           ->allowed());
    }

    public function testViewDeniedNormalUser()
    {
        $user = factory(\App\User::class)->make();
        $spel = factory(\App\Spel::class)->states('denied')->make();
        $this->assertFalse($this->policy->view($user, $spel)
                           ->allowed());
    }

    public function testViewDeniedMaker()
    {
        $user = factory(\App\User::class)->make([
            'id' => 5,
        ]);
        $spel = factory(\App\Spel::class)->states('denied')->make([
            'maker' => 5,
        ]);
        $this->assertTrue($this->policy->view($user, $spel)
                           ->allowed());
    }

    public function testViewDeniedMod()
    {
        // Needs to be saved because of permissions
        $user = factory(\App\User::class)->states('moderator')->create();
        $spel = factory(\App\Spel::class)->states('denied')->make();
        $this->assertTrue($this->policy->view($user, $spel)
                           ->allowed());
    }

    public function testViewVisibleNotCheckedNoUser()
    {
        $spel = factory(\App\Spel::class)->states('visible-not-checked')->make();
        $this->assertTrue($this->policy->view(null, $spel)
                           ->allowed());
    }

    public function testViewVisibleNotCheckedNormalUser()
    {
        $user = factory(\App\User::class)->make();
        $spel = factory(\App\Spel::class)->states('visible-not-checked')->make();
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewVisibleNotCheckedMaker()
    {
        $user = factory(\App\User::class)->make([
            'id' => 6,
        ]);
        $spel = factory(\App\Spel::class)->states('visible-not-checked')
              ->make([
                  'maker' => 6,
              ]);
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }

    public function testViewVisibleNotCheckedMod()
    {
        $user = factory(\App\User::class)->states('moderator')->make();
        $spel = factory(\App\Spel::class)->states('visible-not-checked')->make();
        $this->assertTrue($this->policy->view($user, $spel)
                          ->allowed());
    }
}
