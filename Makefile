update: git composer migrate group

group:
	-chgrp -R www-data *

git:
	git pull origin master

composer: composer.json composer.lock
	composer install

migrate: composer
	php artisan migrate
