<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class TerreinComposer
{
    private $terreinen;

    public function __construct()
    {
        $this->terreinen = \App\Terrein::all();
    }

    public function compose(View $view)
    {
        $view->with('terreinen', $this->terreinen);
    }
}
