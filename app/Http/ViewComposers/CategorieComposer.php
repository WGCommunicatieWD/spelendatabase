<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class CategorieComposer
{
    private $categorieen;

    public function __construct()
    {
        $this->categorieen = \App\Categorie::all();
    }

    public function compose(View $view)
    {
        $view->with('categorieen', $this->categorieen);
    }
}
