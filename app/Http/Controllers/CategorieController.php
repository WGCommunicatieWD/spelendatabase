<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    public function __construct() {
        $this->middleware('can:access admin')->only('store');
    }

    public function show(Categorie $category)
    {
        $spelen = $category->spelen()
                ->where('zichtbaar', true)
                ->paginate(20);

        return view('categories.show')
            ->with('categorie', $category)
            ->with('spelen', $spelen);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'naam' => 'required'
        ]);

        $categorie = Categorie::create($data);
        return redirect()->route('admin');
    }
}
