<?php

namespace App\Http\Controllers;

use Gate;
use App\GameAttachment;
use App\Spel;
use App\Http\Requests\AttachmentUploadRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:view,spel');
        $this->middleware('verified')->except('show');
    }

    public function show(Spel $spel)
    {
        // Toon alle bijlagen _en_ een formulier om toe te voegen
        return view('spelen.attachments')->with('spel', $spel);
    }

    public function store(AttachmentUploadRequest $request, Spel $spel)
    {
        foreach ($request->attachments as $attachment) {
            $filename = $attachment->store('attachments');
            GameAttachment::create([
                'game_id' => $spel->id,
                'filename' => $filename,
                'originalname' => $attachment->getClientOriginalName(),
            ]);
        }

        return redirect()->route('spelen.show', ['spel' => $spel]);
    }

    public function download(Spel $spel, GameAttachment $attachment)
    {
        return Storage::download($attachment->filename, $attachment->originalname);
    }

    public function destroy(Spel $spel, GameAttachment $attachment)
    {
        if (Gate::denies('manage-spel', $spel)) {
            abort(403);
        }

        $attachment->delete();

        // In case multiple games refer to the same file on disk
        if (GameAttachment::where('filename', $attachment->filename)->count() == 0) {
            Storage::delete($attachment->filename);
        }
        return redirect()->route('spelen.attachments', ['spel' => $spel]);
    }
}
