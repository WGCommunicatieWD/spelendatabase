<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Gate;
use Illuminate\Http\Request;
use App\Http\Requests\ManageUserRequest;
use App\Http\Requests\EditProfileRequest;
use App\User;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('can:manage users')
             ->only(['index', 'promote', 'demote', 'verify', 'unverify']);
        $this->middleware('verified')
            ->except('show');
    }

    public function show(User $user) {
        return view('users.show')
            ->with('gebruiker', $user);
    }

    public function index() {
        $gebruikers = \App\User::with('spelen', 'verbond')->get();

        return view('users.index')
            ->with('gebruikers', $gebruikers);
    }

    public function promote(User $user) {
        $user->assignRole('moderator');
        return redirect()->route('users.show', ['user' => $user]);
    }

    public function demote(User $user) {
        $user->removeRole('moderator');
        return redirect()->route('users.show', ['user' => $user]);
    }

    public function verify(User $user) {
        $user->assignRole('verified');
        return redirect()->route('users.show', ['user' => $user]);
    }

    public function unverify(User $user) {
        $user->removeRole('verified');
        return redirect()->route('users.show', ['user' => $user]);
    }

    public function edit(User $user) {
        if (Gate::allows('edit-profile', $user)) {
            return view('users.edit')->with('user', $user);
        } else {
            return redirect()->route('users.show', ['user' => $user]);
        }
    }

    public function update(EditProfileRequest $request, User $user) {
        $data = $request->validated();

        if (!Hash::check($data['currentpassword'], $user->password) && Gate::denies('manage users')) {
            return redirect()->back()->withErrors(['currentpassword' => "Huidig wachtwoord is niet correct."]);
        }

        if ($request->has('password') && $data['password'] != null) {
            $user->password = bcrypt($data['password']);
        }

        $user->email = $data['email'];
        $user->verbond_id = $data['verbond_id'];
        $user->name = $data['name'];
        $user->save();

        return redirect()->route('users.show', ['user' => $user])->with('success', "Je profiel is aangepast.");
    }
}
