<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\{AddToGroupRequest, GameSearchRequest, StoreGameRequest};
use App\Favourite;
use App\Group;
use App\Notifications\FlagReviewed;
use App\Notifications\GameApproved;
use App\Notifications\GameNotApproved;
use App\Notifications\GamesAwaitingApproval;
use App\Repositories\SpelRepository;
use App\Spel;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SpelController extends Controller
{
    private $repository;

    public function __construct(SpelRepository $repository)
    {
        $this->repository = $repository;

        // Only register what has't been done by authorizeResource
        $this->authorizeResource(Spel::class, 'spel');
        $this->middleware('can:viewAny,App\Spel')->only(['search', 'showSearch']);
        $this->middleware('can:view,spel')->only(['print']);
        $this->middleware('can:copy,spel')->only(['copy']);
        $this->middleware('can:love,spel')->only(['love']);
        $this->middleware('can:check,spel')->only(['approve', 'deny']);
        $this->middleware('can:addToGroup,spel')->only(['selectGroup', 'addToGroup']);

        $this->middleware('can:approve games')->only(['clearFlags']);

        $this->middleware('verified')->only(
            ['create', 'store', 'edit',
                'update', 'destroy', 'copy',
                'love', 'selectGroup', 'addToGroup',
                'approve', 'deny',
            ]);
    }

    public function index()
    {
        $spelen = $this->repository
            ->visibleForUserQuery(Auth::user())
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('spelen.index')
            ->with('spelen', $spelen);
    }

    public function show(Request $request, Spel $spel)
    {
        if (!($request->session()->has('viewed' . $spel->id))
            && (!Auth::check() || Gate::allows('stem-spel', $spel))) {
            $spel->views += 1;
            $spel->save();
            session(['viewed' . $spel->id => 'yes']);
        }

        $loves = Gate::allows('stem-spel', $spel) &&
            Auth::user()->favourites()->where('spelId', $spel->id)->exists();

        $spel->load(['comments' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }]);

        return view('spelen.show')->with('spel', $spel)->with('loves', $loves);
    }

    public function approve(Spel $spel)
    {
        $spel->gecontroleerd = true;
        $spel->zichtbaar = true;
        $spel->save();

        if ($spel->maker !== null) {
            $spel->user->notify(new GameApproved($spel));
        }

        return redirect()->route('spelen.show', ['spel' => $spel]);
    }

    public function deny(Spel $spel)
    {
        $spel->gecontroleerd = true;
        $spel->zichtbaar = false;
        $spel->save();

        if ($spel->user !== null)
            $spel->user->notify(new GameNotApproved($spel));

        return redirect()->route('spelen.show', ['spel' => $spel]);
    }

    public function create()
    {
        return view('spelen.create');
    }

    public function store(StoreGameRequest $request)
    {
        $input = $request->validated();
        // remove null values
        $input = array_filter($input, function ($value) {
            return !is_null($value);
        });

        $spel = new Spel($input);
        if (Auth::check()) {
            $spel->maker = Auth::user()->id;
        }

        if (Gate::allows('bypass approval')) {
            $spel->gecontroleerd = true;
            $spel->zichtbaar = true;
        }

        $spel->save();

        if (Gate::denies('bypass approval')) {
            // Send out modmail
            foreach (User::permission('approve games')->get() as $user) {
                $user->notify(new GamesAwaitingApproval());
            }
        }
        return redirect()->route('spelen.show', ['spel' => $spel]);
    }

    public function print(Spel $spel)
    {
        return view('spelen.print')->with('spel', $spel);
    }

    public function edit(Request $request, Spel $spel)
    {
        return view('spelen.edit')
            ->with('spel', $spel);
    }

    public function update(StoreGameRequest $request, Spel $spel)
    {
        $input = $request->validated();

        $spel->update($input);

        // Wanneer aangepast door een niet-mod
        // Moet het opnieuw gekeurd worden
        if (Gate::denies('bypass approval')) {
            $spel->update(['gecontroleerd' => false, 'zichtbaar' => false]);

            if (! $spel->isDraft) {
                $notification = new GamesAwaitingApproval;
                foreach (User::permission('approve games')->get() as $user) {
                    $user->notify($notification);
                }
            }
        }

        $spel->save();

        return redirect()->route('spelen.show', ['spel' => $spel]);
    }

    public function destroy(Request $request, Spel $spel)
    {
        $spel->delete();
        return redirect()->route('home');
    }

    public function search(GameSearchRequest $request)
    {
        $input = $request->validated();
        $request->flash();

        $spelen = $this->repository->visibleForUserQuery(Auth::user());

        if ($request->filled('categorie')) {
            $spelen = $spelen->where('categorieId', $input['categorie']);
        }

        if ($request->filled('terrein')) {
            $spelen = $spelen->where('terreinId', $input['terrein']);
        }

        if ($request->filled('minLeeftijd')) {
            $spelen = $spelen->where('minLeeftijd',
                '>=',
                $input['minLeeftijd']);
        }

        if ($request->filled('maxLeeftijd')) {
            $spelen = $spelen->where('maxLeeftijd',
                '<=',
                $input['maxLeeftijd']);
        }

        if ($request->filled('verbond_id')) {
            $spelen = $spelen->whereHas('user', function ($q) use ($input) {
                $q->where('verbond_id', $input['verbond_id']);
            });
        }

        if ($request->filled('is_jomba') && $input['is_jomba'] != null) {
            $spelen = $spelen->where('is_jomba', $input['is_jomba']);
        }

        return view('spelen.search', ['spelen' => $spelen->paginate(20)]);
    }

    public function love(Spel $spel)
    {
        // We are for sure logged in, see gate above
        $user = Auth::user();

        if (!Favourite::where('userId', $user->id)->where('spelId', $spel->id)->exists()) {
            $favourite = new Favourite;
            $favourite->spelId = $spel->id;
            $favourite->userId = $user->id;
            $favourite->save();

            $spel->score += 1;
            $spel->aantalStemmen += 1;
            $spel->save();
        } else {
            $favourite = Favourite::
            where('userId', $user->id)
                ->where('spelId', $spel->id)
                ->first();

            $favourite->delete();

            $spel->score -= 1;
            $spel->aantalStemmen -= 1;
            $spel->save();
        }

        return redirect()->route('spelen.show', ['spel' => $spel]);
    }

    public function selectGroup(Spel $spel)
    {
        $groups = Group::whereHas('members', function ($query) {
            $query->where('id', '=', Auth::user()->id);
        })->get();

        return view('spelen.addtogroup')
            ->with('spel', $spel)
            ->with('groups', $groups);
    }

    public function addToGroup(AddToGroupRequest $request, Spel $spel)
    {
        $data = $request->validated();
        $group = Group::find($data['group']);
        $this->authorize('update', $group);

        $extra = [];

        if (isset($data['comment'])) {
            $extra['comment'] = $data['comment'];
        }

        if ($data['doschedule']) {
            $extra['scheduled'] = $data['scheduled'];
        }

        $group->games()->attach($spel, $extra);

        return redirect()->route('groups.show', ['group' => $group]);
    }

    // Makes a copy of the existing game for the current user
    public function copy(Spel $spel)
    {
        $newSpel = Auth::user()->spelen()->create([
            'titel' => $spel->titel,
            'minLeeftijd' => $spel->minLeeftijd,
            'maxLeeftijd' => $spel->maxLeeftijd,
            'categorieId' => $spel->categorieId,
            'terreinId' => $spel->terreinId,
            'uitleg' => $spel->uitleg,
            'deelnemers' => $spel->deelnemers,
            'duurMinuten' => $spel->duurMinuten,
            'materiaal' => $spel->materiaal,
            'monis' => $spel->monis,
            'taakverdeling' => $spel->taakverdeling,
            'inkleding' => $spel->inkleding,
            'inclusie' => $spel->inclusie,
            'isDraft' => $spel->isDraft,
            'original_id' => $spel->id,
        ]);

        // Now copy the attachments
        foreach ($spel->attachments as $attachment) {
            $newAttachment = $attachment->replicate();
            $newAttachment->game_id = $newSpel->id;
            $newAttachment->save();
        }
        return redirect()->route('spelen.show', $newSpel);
    }

    public function clearFlags(Spel $spel)
    {
        $email = new FlagReviewed($spel);
        $spel->comments()->where('flag', true)
            ->where('flag_removed_at', '=', null)
            ->get()
            ->each(function (Comment $comment) use ($email) {
                $comment->user->notify($email);
                $comment->flag_removed_at = now();
                $comment->save();
            });

        $spel->user->notify($email);

        return redirect()->back()->with('success', 'Flags zijn verwijderd');
    }

    private function operatorIdToString(int $id): string
    {
        if ($id == 0) {
            return '<=';
        } else if ($id == 1) {
            return '=';
        } else if ($id == 2) {
            return '>=';
        } else {
            return '';
        }
    }
}
