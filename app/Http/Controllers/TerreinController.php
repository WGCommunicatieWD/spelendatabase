<?php

namespace App\Http\Controllers;

use App\Terrein;
use Illuminate\Http\Request;
use Gate;

class TerreinController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:access admin')->only('store');
    }

    public function terrein(Terrein $terrein)
    {
        $spelen = $terrein->spelen()
                ->where('zichtbaar', true)
                ->paginate(20);

        return view('terrein')->with('terrein', $terrein)
            ->with('spelen', $spelen);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'naam' => 'required'
        ]);

        $categorie = new Terrein($data);
        $categorie->save();
        return redirect()->route('admin');
    }
}
