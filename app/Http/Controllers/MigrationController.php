<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

class MigrationController extends Controller
{
    function index(Request $request) {
        if (!Storage::disk('local')->exists('canmigrate')) {
            return response('Not set for migration', 403);
        }

        Storage::disk('local')->delete('canmigrate');
        define('STDIN',fopen("php://stdin","r"));
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
        Artisan::call('db:seed', [
            "--force" => true,
        ]);
        $result = Artisan::call('migrate', [
            '--force' => true,
        ]);

        return response("Return status: $result");
    }
}
