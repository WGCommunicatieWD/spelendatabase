<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Repositories\SpelRepository;

class AdminController extends Controller
{
    private $repository;

    public function __construct(SpelRepository $repository)
    {
        $this->repository = $repository;
        // Require to be authenticated
        $this->middleware(['can:access admin']);
    }

    public function index()
    {
        $tecontroleren = $this->repository->uncheckedQuery()->get();
        $verborgen = $this->repository->hidden();
        $gerapporteerd = $this->repository->reported();

        return view('admin')
            ->with('tecontroleren', $tecontroleren)
            ->with('verborgen', $verborgen)
            ->with('reported', $gerapporteerd);
    }
}
