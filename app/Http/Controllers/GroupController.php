<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\AddMemberRequest;
use App\Http\Requests\GroupStoreRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Spel;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Group::class, 'group');
        $this->middleware('verified')->except(['index', 'show']);
        $this->middleware('can:update,group')->only([
            'newmember',
            'addmember',
            'removemember',
            'removegame',
            'editcomment',
            'updatecomment',
        ]);
    }

    public function index()
    {
        return view('groups.index')->with('groups', Group::visiblePaginated());
    }

    public function create()
    {
        return view('groups.new');
    }

    public function store(GroupStoreRequest $request)
    {
        $data = $request->validated();

        $group = Group::create(['name' => $data['name'], 'public' => $data['public']]);
        if ($data['description'] != null) {
            $group->description = $data['description'];
        }
        $group->save();
        $group->members()->save(Auth::user());
        return redirect()->route('groups.show', ['group' => $group]);
    }

    public function show(Group $group)
    {
        if (Gate::allows('update', $group)) {
            $candidates = \App\User::whereDoesntHave('groups', function ($query) use ($group) {
                $query->where('id', '=', $group->id);
            })->where('id', '!=', Auth::user()->id)->get();
            return view('groups.show')
                ->with('candidates', $candidates)
                ->with('group', $group);
        }
        return view('groups.show')->with('group', $group);
    }

    public function edit(Group $group)
    {
        return view('groups.edit')->with('group', $group);
    }

    public function update(GroupStoreRequest $request, Group $group)
    {
        $data = $request->validated();
        $group->name = $data['name'];
        $group->public = $data['public'];
        if ($data['description'] != null) {
            $group->description = $data['description'];
        }
        $group->save();

        return redirect()->route('groups.show', ['group' => $group])->with('success', 'Je wijzigingen zijn opgeslagen.');
    }

    public function newmember(Group $group)
    {
        $candidates = \App\User::whereDoesntHave('groups', function ($query) use ($group) {
            $query->where('id', '=', $group->id);
        })->where('id', '!=', Auth::user()->id)->get();
        return view('groups.newmember')
            ->with('group', $group)
            ->with('candidates', $candidates);
    }

    public function addmember(AddMemberRequest $request, Group $group)
    {
        $data = $request->validated();
        foreach ($data['users'] as $id) {
            $user = \App\User::find($id);
            $group->members()->attach($user);
        }

        return redirect()->route('groups.show', ['group' => $group]);
    }

    public function removegame(Request $request, Group $group, Spel $spel)
    {
        if ($group->games->where('id', '=', $spel->id)->count() < 1) {
            return redirect()
                ->route('groups.show', ['group' => $group]);
        }

        $group->games()->detach($spel);
        return redirect()
            ->route('groups.show', ['group' => $group])
            ->with('success', 'Het spel is uit de groep verwijderd.');
    }

    public function removemember(Request $request, Group $group, User $user)
    {
        if ($group->members->where('id', '=', $user->id)->count() < 1) {
            return redirect()
                ->route('groups.show', ['group' => $group]);
        }

        $group->members()->detach($user);
        return redirect()
            ->route('groups.show', ['group' => $group])
            ->with('success', 'De persoon is uit de groep verwijderd.');
    }

    public function editcomment(Group $group, Spel $spel)
    {
        $game = $group->games()->find($spel->id);
        return view('groups.editcomment')->with('group', $group)->with('game', $game);
    }

    public function updatecomment(UpdateCommentRequest $request, Group $group, Spel $spel)
    {
        $data = $request->validated();
        $game = $group->games()->find($spel->id);
        $game->pivot->comment = $data['comment'];
        if ($data['doschedule']) {
            $game->pivot->scheduled = $data['scheduled'];
        }
        $game->pivot->save();
        return redirect()->route('groups.show', ['group' => $group]);
    }
}
