<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Verbond;

class VerbondController extends Controller
{
    public function index() {
        $verbonden = Verbond::all();
        return view('verbonden.index')->with('verbonden', $verbonden);
    }

    public function show(Verbond $verbond) {
        return view('verbonden.show')->with('verbond', $verbond);
    }
}
