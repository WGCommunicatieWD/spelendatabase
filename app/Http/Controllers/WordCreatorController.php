<?php

namespace App\Http\Controllers;
use App\Spel;
use Illuminate\Http\Request;
use GrahamCampbell\Markdown\Facades\Markdown;

class WordCreatorController extends Controller
{
    public function create(Request $request, Spel $spel)
    {
        // Escape output!
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $word = new \PhpOffice\PhpWord\PhpWord();

        $spaceAfterStyle = [
            'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(12),
        ];

        $section = $word->addSection();

        // Paginanummering
        $footer = $section->addFooter();
        $footer->addText("Via de Kazou Spelendatabase");
        $footer->addPreserveText('Pagina {PAGE}');

        // Header met logo
        $header = $section->addHeader();
        $header->addText("{$spel->titel}");
        $header->addImage(storage_path("kazou-logo.png"), [
            'width' => 75,
            'align' => 'right',
        ]);

        //Title and maker in het word.
        $word->addTitleStyle(0, ['size' => 25, 'bold' => true]);
        $section->addTitle($spel->titel, 0);
        $section->addText('Door ' . $spel->user->name, ['size' => 9], $spaceAfterStyle);

        //tabel voor de info
        $tableStyle = [
            'width' => 50 * 50,
            'cellMargin' => 50,
            'align' => 'center',
        ];

        //opmaak van de categorie van het info.
        $FirstStyle = array('size' => 10, 'bold' => true);
        //De info die de maker onze geeft.
        $SecondStyle = array('size' => 10);

        $GrootCell = 3000;
        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell($GrootCell)->addText('Categorie:', $FirstStyle);
        $table->addCell($GrootCell)->addText('Terrein:', $FirstStyle);

        $table->addRow();
        $table->addCell($GrootCell)->addText($spel->categorie->naam, $SecondStyle);
        $table->addCell($GrootCell)->addText($spel->terrein->naam, $SecondStyle);

        $table->addRow();
        $table->addCell($GrootCell)->addText("Aantal moni's:", $FirstStyle);
        $table->addCell($GrootCell)->addText('Aantal deelnemers:', $FirstStyle);

        $table->addRow();
        $table->addCell($GrootCell)->addText(strval($spel->monis), $SecondStyle);
        $table->addCell($GrootCell)->addText('Ongeveer ' . $spel->deelnemers, $SecondStyle);

        $table->addRow();
        $table->addCell($GrootCell)->addText('Leeftijd:', $FirstStyle);
        $table->addCell($GrootCell)->addText('Duur:', $FirstStyle);

        $table->addRow();
        $table->addCell($GrootCell)->addText('Van ' . $spel->minLeeftijd . ' tot ' . $spel->maxLeeftijd . ' jaar', $SecondStyle);
        $table->addCell($GrootCell)->addText($spel->duurMinuten . ' minuten', $SecondStyle);

        //opmakke van de tussentitels
        $word->addTitleStyle(1,
                             ['size' => 14, 'bold' => true],
                             [
                                 'spaceBefore' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(12),
                             ]);

        if (!is_null($spel->inkleding))
        {
            $section->addTitle('Inkleding', 1);
            $inkleding = Markdown::convertToHtml($spel->inkleding);
            \PhpOffice\PhpWord\Shared\Html::addHtml($section, $inkleding);
        }

        $section->addTitle('Uitleg', 1);
        $Uitleg = Markdown::convertToHtml($spel->uitleg);
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $Uitleg);

        if (!is_null($spel->taakverdeling))
        {
            $section->addTitle('Taakverdeling', 1);
            $taakverdeling = Markdown::convertToHtml($spel->taakverdeling);
            \PhpOffice\PhpWord\Shared\Html::addHtml($section, $taakverdeling);
        }

        if (!is_null($spel->inclusie))
        {
            $section->addTitle('Inclusie', 1);
            $inclusie = Markdown::convertToHtml($spel->inclusie);
            \PhpOffice\PhpWord\Shared\Html::addHtml($section, $inclusie);
        }

        if (!is_null($spel->materiaal))
        {
            $section->addTitle('Materiaal', 1);
            $materiaal = Markdown::convertToHtml($spel->materiaal);
            \PhpOffice\PhpWord\Shared\Html::addHtml($section, $materiaal);
        }

        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($word, 'Word2007');
        $filename = preg_replace('/[^A-Za-z0-9_\-]/', '_', $spel->titel) . '.docx';
        $objectWriter->save(storage_path($filename));

        return response()->download(storage_path($filename))->deleteFileAfterSend(true);;
    }
}
