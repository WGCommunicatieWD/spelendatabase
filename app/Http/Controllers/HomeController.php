<?php

namespace App\Http\Controllers;

use Auth;
use App\Repositories\SpelRepository;

class HomeController extends Controller
{
    private $repository;

    public function __construct(SpelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke()
    {
        $recente = $this->repository
                 ->visibleForUserQuery(Auth::user())
                 ->orderByDesc('created_at')
            ->limit(5)->get();

        $hoogste = $this->repository
                 ->visibleForUserQuery(Auth::user())
                 ->orderByDesc('score')
            ->limit(5)->get();

        $aantal = $this->repository
                ->visibleForUserQuery(Auth::user())
                ->count();

        return view('welcome')
            ->with('gameCount', $aantal)
            ->with('recente', $recente)
            ->with('hoogsteRank', $hoogste);
    }
}
