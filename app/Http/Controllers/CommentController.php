<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Notifications\Flagged;
use App\Notifications\NewCommentOnGame;
use App\Spel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(StoreCommentRequest $request, Spel $spel)
    {
        $comment = new Comment();
        $comment->fill($request->validated());
        $comment->user()->associate(Auth::user());
        $spel->comments()->save($comment);

        $spel->user->notify(new NewCommentOnGame($spel));

        if ($comment->flag) {
            $email = new Flagged($spel);
            $spel->user->notify($email);

            foreach (User::permission('approve games')->get() as $user)
            {
                $user->notify($email);
            }
        }

        return redirect()->route('spelen.show', $spel);
    }

    public function update(Request $request, Comment $comment)
    {
        //
    }

    public function destroy(Comment $comment)
    {
        //
    }
}
