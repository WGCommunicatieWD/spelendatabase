<?php

namespace App\Http\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class AttachmentUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('manage-spel', $this->spel);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attachments.*' => 'file|mimes:jpeg,bmp,png,docx,doc,pptx,ppt,xlsx,xls,odt,odp,pdf,mp4,avi,gif,mp3,wav|max:2000',
        ];
    }
}
