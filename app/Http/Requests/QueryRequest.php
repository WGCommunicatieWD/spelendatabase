<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QueryRequest extends FormRequest
{
    public function all($keys = null)
    {
        $data = parent::all($keys);
        foreach ($this->query->all() as $key => $value) {
            $data[$key] = $value;
        }
        return $data;
    }
}
