<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameSearchRequest extends QueryRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categorie' => 'nullable|exists:categorieen,id',
            'terrein' => 'nullable|exists:terreinen,id',
            'minLeeftijd' => 'bail|nullable|integer|min:0|lte:maxLeeftijd',
            'maxLeeftijd' => 'bail|nullable|integer|min:0|gte:minLeeftijd',
            'duur' => 'nullable|integer|min:0',
            'verbond_id' => 'nullable|integer|exists:verbonds,id',
            'is_jomba' => 'boolean|nullable',
        ];
    }
}
