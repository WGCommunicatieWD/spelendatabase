<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'minLeeftijd' => 'required|min:0|max:255|integer|lte:maxLeeftijd',
            'maxLeeftijd' => 'required|min:0|max:255|integer|gte:minLeeftijd',
            'titel' => 'required|max:200|string',
            'uitleg' => 'required|string',
            'duurMinuten' => 'required|min:0|max:65535|integer',
            'deelnemers' => 'required|integer',
            'monis' => 'min:0|max:255|integer|nullable',
            'materiaal' => 'nullable|string',
            'inkleding' => 'nullable|string',
            'inclusie' => 'nullable|string',
            'taakverdeling' => 'nullable|string',
            'terreinId' => 'exists:terreinen,id',
            'categorieId' => 'exists:categorieen,id',
            'isDraft' => 'boolean|nullable',
            'is_jomba' => 'boolean|nullable',
        ];
    }
}
