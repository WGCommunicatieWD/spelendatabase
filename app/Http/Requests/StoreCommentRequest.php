<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'content' => 'string|required',
            'restricted_to_author' => 'boolean',
            'flag' => 'boolean',
        ];
    }
}
