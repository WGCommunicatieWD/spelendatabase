<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;

    protected $fillable = [
        'name', 'email', 'password', 'verbond_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function spelen() {
        return $this->hasMany('App\Spel', 'maker');
    }

    public function favourites()
    {
        return $this->hasMany('App\Favourite', 'userId');
    }

    public function verbond()
    {
        return $this->belongsTo('App\Verbond');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_members', 'user_id', 'group_id');
    }

    public function comments(): HasMany
    {
        return $this->hasMany('App\Comment');
    }
}
