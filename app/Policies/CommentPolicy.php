<?php

namespace App\Policies;

use App\Comment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        // TODO
    }

    public function view(?User $user, Comment $comment)
    {
        if ($comment->restricted_to_author === false) {
            return true;
        }

        if (is_null($user)) {
            return false;
        }

        if ($user->id === $comment->user_id) {
            return true;
        }

        if ($user->id === $comment->game->maker) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        return $user->hasVerifiedEmail();
    }

    public function update(User $user, Comment $comment)
    {
        return $user->id === $comment->user_id;
    }

    public function delete(User $user, Comment $comment)
    {
        if ($user->can('moderate comments')) {
            return true;
        }

        if ($user->id === $comment->user_id) {
            return true;
        }

        return false;
    }
}
