<?php

namespace App\Policies;

use App\Spel;
use App\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class SpelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any spels.
     *
     * Rules: anyone can view the index,
     * but we _must_ filter which ones.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(?User $user)
    {
        return Response::allow();
    }

    /**
     * Determine whether the user can view the spel.
     * Rules: Via URL, non-checked are visible to any user,
     * except if disapproved, even drafts.
     * Creator can always view a game.
     * Via search: only approved games
     * Admin and mod can always view
     *
     * @param  \App\User  $user
     * @param  \App\Spel  $spel
     * @return mixed
     */
    public function view(?User $user, Spel $spel)
    {
        // Allow if you made the game
        if (! is_null($user) && ! is_null($spel->maker)
            && $user->id == $spel->maker) {
            return Response::allow();
        }

        // Allow if you can approve games, for review purposes
        if ((! is_null($user)) && $user->can('approve games')) {
            return Response::allow();
        }

        if ($spel->gecontroleerd && $spel->zichtbaar == false) {
            return Response::deny('Dit spel is afgekeurd door een moderator.');
        }


        return Response::allow();
    }

    public function discover(?User $user, Spel $spel): Response
    {
        // Allow if you made the game
        if (! is_null($user) && ! is_null($spel->maker)
            && $user->id == $spel->maker) {
            return Response::allow();
        } else if ($spel->isDraft) {
            return Response::deny("Dit spel is nog een kladversie.");
        } else {
            return $this->view($user, $spel);
        }
    }

    /**
     * Determine whether the user can create spels.
     *
     * Rules: all users with verified e-mail can create a game.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(?User $user)
    {
        if (is_null($user)) {
            return $this->deny("Je moet je eerst registreren of inloggen.");
        }

        if ($user->hasVerifiedEmail()) {
            return Response::allow();
        }

        return Response::deny('Je moet je e-mail verifiëren.');
    }

    /**
     * Determine whether the user can update the spel.
     *
     * Rules: If the user made the game, allow.
     * If the user is an admin, allow.
     *
     * @param  \App\User  $user
     * @param  \App\Spel  $spel
     * @return mixed
     */
    public function update(User $user, Spel $spel)
    {
        if ($user->id == $spel->maker) {
            return Response::allow();
        }

        if ($user->can('edit any game')) {
            return Response::allow();
        }

        // Not an admin or mod, so this applies.
        return Response::deny('Je mag enkel eigen spelen bewerken.');
    }

    /**
     * Determine whether the user can delete the spel.
     *
     * Rules: same as update
     *
     * @param  \App\User  $user
     * @param  \App\Spel  $spel
     * @return mixed
     */
    public function delete(User $user, Spel $spel)
    {
        return $this->update($user, $spel);
    }

    /**
     * Determine whether the user can restore the spel.
     *
     * Rules: same as delete.
     *
     * @param  \App\User  $user
     * @param  \App\Spel  $spel
     * @return mixed
     */
    public function restore(User $user, Spel $spel)
    {
        return $this->delete($user, $spel);
    }

    /**
     * Determine whether the user can permanently delete the spel.
     *
     * @param  \App\User  $user
     * @param  \App\Spel  $spel
     * @return mixed
     */
    public function forceDelete(User $user, Spel $spel)
    {
        return $user->can('force-delete posts')
            ? Response::allow()
            : Response::deny('Enkel een admin kan spelen permanent verwijderen.');
    }

    /**
     * Determine whether a user can copy a game.
     *
     * Rules: can create a game and view the requested game.
     *
     */
    public function copy(User $user, Spel $spel)
    {
        if ($this->view($user, $spel) && $this->create($user)) {
            return Response::allow();
        }
        return Response::deny('Je moet een spel kunnen bekijken en aanmaken.');
    }

    /**
     * Determine whether a user can mark a game as loved.
     *
     * Rules: Logged in and not the creator, can view the game.
     *
     */
    public function love(User $user, Spel $spel)
    {
        if ($user->id == $spel->maker) {
            return Response::deny('Je kan je eigen spelen niet als favoriet markeren.');
        }

        return $this->view($user, $spel);
    }

    /**
     * Determine whether a user can approve or deny the game.
     *
     * Rules: admin or moderator and not a draft
     *
     */
    public function check(User $user, Spel $spel)
    {
        if ($spel->isDraft) {
            return Response::deny('Dit spel is nog een kladversie');
        }

        if ($user->can('approve games')) {
            return Response::allow();
        }

        return Response::deny('Enkel moderators en admins kunnen spelen controleren.');
    }

    /**
     * Determine whether a user can add a game to a group.
     *
     * Rules: can view the game (open for debate)
     *
     */
    public function addToGroup(User $user, Spel $spel)
    {
        return $this->view($user, $spel);
    }
}
