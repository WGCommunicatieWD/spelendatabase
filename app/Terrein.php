<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Terrein
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Terrein newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Terrein newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Terrein query()
 * @mixin \Eloquent
 */
class Terrein extends Model
{
    public $timestamps = false;
    protected $table = 'terreinen';

    protected $fillable = [
        'naam'
    ];

    public function spelen()
    {
        return $this->hasMany('App\Spel', 'terreinId');
    }
}
