<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthorizationException) {
            if (Auth::guest()) {
                return redirect()->guest(route('login'))->with('error', 'Je moet je inloggen om deze pagina te bekijken.');
            } else if (Auth::user()->hasVerifiedEmail() == false) {
                return redirect()->guest(route('verification.notice'));
            }
        }
        return parent::render($request, $exception);
    }
}
