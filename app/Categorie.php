<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Categorie
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categorie newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categorie newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categorie query()
 * @mixin \Eloquent
 */
class Categorie extends Model
{
    public $timestamps = false;
    protected $table = 'categorieen';

    protected $fillable = [
        'naam'
    ];

    public function spelen()
    {
        return $this->hasMany('App\Spel', 'categorieId');
    }
}
