<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favourite extends Model
{
    use SoftDeletes;

    public static function findForUser(User $user) {
        $spelen = [];
        $favourites = Favourite::where('userId', $user->id)->get();

        foreach ($favourites as $favourite) {
            array_push($spelen, Spel::find($favourite->spelId));
        }

        return $spelen;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }

    public function spel()
    {
        return $this->belongsTo('App\Spel', 'spelId');
    }
}
