<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'public', 'description'];

    public function members(): BelongsToMany
    {
        return $this->belongsToMany('App\User', 'group_members', 'group_id', 'user_id');
    }

    public function games(): BelongsToMany
    {
        return $this->belongsToMany('App\Spel', 'group_games', 'group_id', 'spel_id')
            ->withPivot('comment', 'scheduled');
    }

    public function hasUser(User $user): bool
    {
        return $this->members()->where('id', $user->id)->exists();
    }

    public function getVisibleGamesAttribute(): Collection
    {
        $allGames = $this->games()->orderBy('scheduled', 'asc')->get();

        if (Gate::allows('group-member', $this)) {
            // If you are a member, show all games
            return $allGames;
        } else {
            // If not, show only games you should be able to see
            // and no drafts
            return $allGames->filter(function($spel) {
                return Gate::allows('discover', $spel);
            });
        }
    }

    public static function visiblePaginated()
    {
        if (Auth::check() && Auth::user()->hasRole('admin')) {
            return Group::paginate(20);
        } elseif (Auth::check()) {
            return Group::where('public', true)->orWhereHas('members', function($query) {
                $query->where('id', '==', Auth::user()->id);
            })->paginate(20);
        } else {
            return Group::where('public', true)->paginate(20);
        }
    }
}
