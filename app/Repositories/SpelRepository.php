<?php

namespace App\Repositories;

use App\Spel;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class SpelRepository
{
    public function visibleQuery()
    {
        return Spel::where(function ($query) {
            $query->where('gecontroleerd', true)
                  ->where('zichtbaar', true)
                  ->where('isDraft', false);
        });
    }

    public function visible()
    {
        return $this->visibleQuery()->get();
    }

    public function visibleForUserQuery(?User $user)
    {
        if (is_null($user)) {
            return $this->visibleQuery();
        }

        if ($user->can('approve games')) {
            return Spel::where(function ($query) use ($user) {
                $query->where('isDraft', false)
                      ->orWhere('maker', $user->id);
            });
        }
        return $this->visibleQuery()
                    ->orWhere('maker', $user->id);
    }

    public function visibleForUser(?User $user)
    {
        return $this->visibleForUserQuery($user)->get();
    }

    public function uncheckedQuery()
    {
        return Spel::where(function ($query) {
            $query->where('isDraft', false)
                  ->where('gecontroleerd', false);
        });
    }

    public function hidden()
    {
        return Spel::where(function ($query) {
            $query->where('gecontroleerd', true)
                  ->where('zichtbaar', false)
                  ->where('isDraft', false);
                })->get();
    }

    public function reported()
    {
        return Spel::whereHas('comments', function (Builder $query) {
            $query->where('flag', true)
                ->where('flag_removed_at', '=', null);
        })
        ->get();
    }
}
