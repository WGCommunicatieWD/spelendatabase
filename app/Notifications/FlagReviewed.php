<?php

namespace App\Notifications;

use App\Spel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FlagReviewed extends Notification
{
    use Queueable;

    public $spel;

    public function __construct(Spel $spel)
    {
        $this->spel = $spel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Flag gecontroleerd")
            ->line("Het spel {$this->spel->titel} was gerapporteerd.")
            ->line("Een moderator heeft het gecontroleerd.")
            ->action('Bekijk het spel', route('spelen.show', $this->spel));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
