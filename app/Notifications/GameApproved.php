<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GameApproved extends Notification
{
    use Queueable;

    /* @var Spel */
    public $spel;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Spel $spel)
    {
        $this->spel = $spel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $titel = $this->spel->titel;

        return (new MailMessage)
                    ->subject('Spel goedgekeurd')
                    ->greeting('Proficiat!')
                    ->line("Je spel \"$titel\" is goedgekeurd!")
                    ->action('Bekijk Spel', route('spelen.show', ['spel' => $this->spel]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
