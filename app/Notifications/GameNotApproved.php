<?php

namespace App\Notifications;

use App\Spel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class GameNotApproved extends Notification
{
    use Queueable;

    /* @var Spel */
    public $spel;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Spel $spel)
    {
        $this->spel = $spel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $titel = $this->spel->titel;
        return (new MailMessage)
                    ->subject('Spel afgekeurd')
                    ->line("Je spel $titel is afgekeurd.")
                    ->line("Bekijk nu of je je spel kan verbeteren!")
                    ->action('Ga naar je spel', route('spelen.show', ['spel' => $this->spel]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
