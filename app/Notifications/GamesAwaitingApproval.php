<?php

namespace App\Notifications;

use App\Repositories\SpelRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GamesAwaitingApproval extends Notification
{
    use Queueable;

    private $repository;
    private $count;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->repository = resolve('App\Repositories\SpelRepository');
        $this->count = $this->repository->uncheckedQuery()->count();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("$this->count spelen wachten op goedkeuring")
                    ->line("$this->count spelen wachten op goedkeuring.")
                    ->line('Kan je er even naar kijken als je tijd hebt?')
                    ->action('Naar de Admin-interface', route('admin'))
                    ->line('Al bedankt!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
