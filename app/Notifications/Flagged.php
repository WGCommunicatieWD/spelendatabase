<?php

namespace App\Notifications;

use App\Spel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Flagged extends Notification
{
    use Queueable;

    public $spel;

    public function __construct(Spel $spel)
    {
        $this->spel = $spel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Spel gerapporteerd")
            ->line("Het spel {$this->spel->titel} is gerapporteerd.")
            ->line("Een moderator zal het controleren.")
            ->action('Bekijk het spel', route('spelen.show', $this->spel));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
