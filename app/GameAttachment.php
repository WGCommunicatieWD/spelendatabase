<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameAttachment extends Model
{
    protected $fillable = ['game_id', 'filename', 'originalname'];

    public function game()
    {
        return $this->belongsTo('App\Spel');
    }
}
