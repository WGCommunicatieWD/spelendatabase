<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Wildcard to add to all views.
        View::composer(
            '*', 'App\Http\ViewComposers\TerreinComposer'
        );

        View::composer(
            '*', 'App\Http\ViewComposers\CategorieComposer'
        );
    }
}
