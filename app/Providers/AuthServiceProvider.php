<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Group' => 'App\Policies\GroupPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Admins can do all the things
        // Gate::before(function ($user, $ability) {
        //     if ($user->hasRole('admin')) {
        //         return true;
        //     }
        // });

        // Used for editing and deleting
        Gate::define('manage-spel', function($user, $spel) {
            return $spel->maker == $user->id || $user->hasRole('admin');
        });

        Gate::define('stem-spel', function($user, $spel) {
            return $user->id !== $spel->maker;
        });

        Gate::define('edit-profile', function($user, $profileuser) {
            return $user->id == $profileuser->id;
        });

        Gate::define('group-member', function($user, $group) {
            return $group->members->where('id', '==', $user->id)->count() >= 1;
        });
    }
}
