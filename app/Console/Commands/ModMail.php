<?php

namespace App\Console\Commands;

use App\Notifications\GamesAwaitingApproval;
use App\Repositories\SpelRepository;
use App\User;
use Illuminate\Console\Command;

class ModMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:modmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stuurt een mail naar alle mods met het aantal goed te keuren spelen.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Getting users...");

        if ((new SpelRepository)->uncheckedQuery()->count() <= 0) {
            $this->info('Nothing to send. Done.');
            return 0;
        }

        $this->info("Sending email...");
        $notification = new GamesAwaitingApproval;
        foreach (User::permission('approve games')->get() as $user)
        {
            $user->notify($notification);
        }

        $this->info("Done.");
    }
}
