<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newuser:admin {username} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an admin account.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('username');
        $email = $this->argument('email');
        $password = $this->ask('What is the password for the new user?');
        $user = new User();
        $user->password = Hash::make($password);
        $user->email = $email;
        $user->name = $name;
        $user->email_verified_at = date('Y-m-d');
        $user->save();
        $user->assignRole('admin');

        $this->info('Successfully created the user. You can now log in.');
    }
}
