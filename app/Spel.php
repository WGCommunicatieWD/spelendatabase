<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Spel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Spel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Spel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Spel query()
 * @mixin \Eloquent
 */
class Spel extends Model
{
    use SoftDeletes;

    protected $table = 'spelen';
    protected $fillable = [
        'titel', 'minLeeftijd', 'maxLeeftijd', 'categorieId', 'terreinId', 'uitleg', 'deelnemers', 'original_id',
        'duurMinuten', 'materiaal', 'monis', 'taakverdeling', 'inkleding', 'inclusie', 'isDraft', 'is_jomba',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User', 'maker');
    }

    public function categorie(): BelongsTo
    {
        return $this->belongsTo('App\Categorie', 'categorieId');
    }

    public function original(): BelongsTo
    {
        return $this->belongsTo('App\Spel');
    }

    public function derivatives(): HasMany
    {
        return $this->hasMany('App\Spel', 'original_id');
    }

    public function favourites(): HasMany
    {
        return $this->hasMany('App\Favourite', 'spelId');
    }

    public function terrein(): BelongsTo
    {
        return $this->belongsTo('App\Terrein', 'terreinId');
    }

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany('App\Group', 'group_games', 'spel_id', 'group_id')
            ->withPivot('comment', 'scheduled');
    }

    public function attachments(): HasMany
    {
        return $this->hasMany('App\GameAttachment', 'game_id');
    }

    public function comments(): HasMany
    {
        return $this->hasMany('App\Comment', 'game_id');
    }

    public function getPublicAttribute(): bool
    {
        return $this->zichtbaar && $this->gecontroleerd && !($this->isDraft);
    }

    public function getVisibleByUrlAttribute(): bool
    {
        if ($this->gecontroleerd && ($this->zichtbaar == false)) {
            return false;
        }

        return true;
    }

    public function getFlaggedAttribute(): bool
    {
        return $this->comments()->where('flag', true)
                ->where('flag_removed_at', '=', null)
                ->count() > 0;
    }

    public static function boot() {
        parent::boot();

        static::deleting(function ($obj) {
            foreach($obj->favourites()->get() as $favourite) {
                $favourite->delete();
            }
        });

        static::restored(function ($obj) {
            foreach ($obj->favourites()->withTrashed()->get() as $favourite) {
                $favourite->restore();
            }
        });
    }
}
