<?php

namespace App;

use App\Notifications\FlagReviewed;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    protected $fillable = ['content', 'restricted_to_author', 'flag'];

    protected $casts = [
        'restricted_to_author' => 'boolean',
        'flag' => 'boolean',
        'flag_removed_at' => 'timestamp',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User');
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo('App\Spel', 'game_id');
    }
}
