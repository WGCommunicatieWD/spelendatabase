@extends('layouts.app')

@section('title', __('Forbidden'))
@section('content')
    <div class="alert error center">
        <h1>{{ __($exception->getMessage() ?: 'Niet toegestaan') }}</h1>
        <p>
            Je hebt geen toegang tot deze pagina.
            Wil je terug naar <a href="{{ route('home') }}">home</a>
            of <a href="{{ route('spelen.zoek') }}">zoeken</a>?
        </p>
    </div>
@endsection
