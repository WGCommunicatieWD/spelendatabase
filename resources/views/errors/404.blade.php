@extends('layouts.app')

@section('title', __('Not Found'))

@section('content')
    <section class="alert error center">
        <h1>Pagina niet gevonden</h1>
        Wil je terug naar <a href="{{ route('home') }}">home</a>
        of <a href="{{ route('spelen.zoek') }}">zoeken naar een spel</a>?
    </section>
@endsection
