@extends('layouts.app')

@section('title', 'Groepen')

@section('content')
    <section class="center">
        <h1>Alle groepen</h1>
        @auth
        <a class="btn primary" href="{{ route('groups.create') }}">Nieuwe groep</a>
        @endauth
    </section>

    @component('components.shortgroups', ['groups' => $groups])
    @endcomponent
@endsection
