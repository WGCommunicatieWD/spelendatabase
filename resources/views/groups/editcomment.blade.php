@extends('layouts.app')

@section('title', $group->name)

    @section('extra-header')
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    @endsection

    @section('content')
        <section class="center">

            <h1>Bewerk opmerking voor {{ $game->titel }}</h1>

            <form method="POST" action="{{ route('groups.updatecomment', ['group' => $group, 'spel' => $game]) }}">
                @csrf
                @method('PUT')

                <label for="comment">Opmerking</label>
                <textarea style="resize:vertical;" @if($errors->has('comment')) class="errorinput" @endif cols="80" rows="5" id="comment" name="comment">{{ old('description', $game->pivot->comment) }}</textarea>

                    @if ($errors->has('comment'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('comment') }}
                    </div>
                    @endif

                    <label for="scheduled">Wanneer is dit spel gepland? (optioneel)</label>
                    <input id="scheduled" class="flatpickr flatpickr-input" name="scheduled" type="text" value="{{ old('scheduled', $game->pivot->scheduled) }}" readonly="readonly" />
                    <label for="doschedule">Plan het spel?</label>
                    <input name="doschedule" type="hidden" value="0"/>
                    <input id="doschedule" name="doschedule" type="checkbox" value="1" @if (old('doschedule', $game->pivot->scheduled != null)) checked @endif/>

                        @if ($errors->has('scheduled'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('scheduled') }}
                    </div>
                        @endif

                        <div class="group">
                            <button type="submit">
                                <span class="fas fa-save"></span>
                                Opslaan
                            </button>
                        </div>
            </form>

        @endcomponent
    @endsection

    @section('scriptarea')
        <script>
            $(document).ready(function() {
                $('#scheduled').flatpickr({
                    enableTime: true,
                    dateFormat: "Y-m-d H:i",
                    time_24hr: true,
                });
            });
        </script>
    @endsection
