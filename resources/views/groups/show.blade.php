@extends('layouts.app')

@section('title'){{ $group->name }}@endsection

@section('content')
    <section class="center">
        <h1>{{ $group->name }}</h1>
        <p>
            {{ $group->description }}
        </p>
        <p>
            Je kan een spel toevoegen vanuit de spelpagina.
        </p>
    </section>

    @foreach ($group->visibleGames as $game)
        <section class="center">
            <h2>
                <a href="{{route('spelen.show', $game)}}">
                    {{ $game->titel }}
                </a>
                @can('update', $group)
                    <a href="{{ route('groups.editcomment', ['group' => $group, 'spel' => $game]) }}">
                        <span class="fa fa-pencil-alt"></span>
                    </a>
                @endcan
            </h2>
            @unless (is_null($game->maker))
                <p>
                    <em>Door @include('components.gebruiker', ['gebruiker' => $game->user])</em>
                </p>
            @endunless
            <p>
                {{ $game->duurMinuten }} minuten, {{ $game->deelnemers }} deelnemers
            </p>
            @unless(is_null($game->pivot->scheduled))
                <p>
                    Gepland op: {{ $game->pivot->scheduled }}
                </p>
            @endunless
            @unless (is_null($game->pivot->comment))
                <p>
                    {{ $game->pivot->comment }}
                </p>
            @endunless
            <p><a href="{{ route('spelen.show', $game) }}">Lees meer...</a></p>
            <form class="d-inline" action="{{ route('groups.game', ['group' => $group, 'spel' => $game]) }}"
                  method="post">
                @method('DELETE')
                @csrf
                <div class="group">
                    <button class="danger" type="submit">
                        <span class="fas fa-trash-alt"></span>
                    </button>
                </div>
            </form>
        </section>
    @endforeach
@endsection

@section('right-bar')
    <section class="center">
        <h2>Leden</h2>
        <ul>
            @foreach ($group->members as $member)
                @can('update', $group)
                    <form method="post" action="{{ route('groups.member', ['group' => $group, 'user' => $member]) }}">
                        @csrf
                        @method('DELETE')
                        <li>
                            <a href="{{ route('users.show', $member) }}">
                                {{ $member->name }}
                            </a>
                            <button type="submit" class="transparent danger">
                                <span class="fas fa-trash-alt"></span>
                            </button>
                        </li>
                    </form>
                @else
                    <li>
                        <a href="{{ route('users.show', $member) }}">
                            {{ $member->name }}
                        </a>
                    </li>
                @endcan
            @endforeach
        </ul>
        @can('update', $group)
            <p>
                @if (false)
                    <a class="btn primary" href="{{ route('groups.newmember', $group) }}">
                        <span class="fa fa-user-plus"></span> Voeg gebruiker toe
                    </a>
                @endif
                <add-user-to-group
                    :options='@json($candidates)'
                    name="users[]"
                    href="{{ route('groups.newmember', $group) }}"
                ></add-user-to-group>
            </p>
        @endcan
    </section>
@endsection

@can('update', $group)
@section('left-bar')
    <section class="center">
        <p>
            <a class="btn primary" href="{{ route('groups.edit', $group) }}">
                <span class="fa fa-pencil-alt"></span> Bewerk deze groep
            </a>
        </p>
    </section>
@endsection
@endcan
