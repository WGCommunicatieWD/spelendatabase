@extends('layouts.app')

@section('title', $group->name)

    @section('content')
        <section class="center">

            <h1>Bewerk groep</h1>

            <form method="POST" action="{{ route('groups.update', ['group' => $group]) }}">
                @csrf
                @method('PUT')

                <label for="name">{{ __('Name') }}</label>
                <input id="name" type="text" {{ $errors->has('name') ? ' errorinput' : '' }}" name="name" value="{{ old('name', $group->name) }}" required autofocus>

                @if ($errors->has('name'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('name') }}
                    </div>
                @endif

                <label for="description">Beschrijving</label>
                <textarea style="resize:vertical;" @if($errors->has('description')) class="errorinput" @endif cols="80" rows="5" id="description" name="description">{{ old('description', $group->description) }}</textarea>

                    @if ($errors->has('description'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('description') }}
                    </div>
                    @endif

                    <label for="public">Is de groep publiek?</label>
                    <input name="public" type="hidden" value="0"/>
                    <input name="public" type="checkbox" value="1" @if($group->public) checked @endif />

                    @if ($errors->has('public'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('public') }}
                    </div>
                    @endif

                        <div class="group">
                            <button type="submit">
                                <span class="fas fa-save"></span>
                                Opslaan
                            </button>
                        </div>
            </form>

        </section>
    @endsection
