@extends('layouts.app')

@section('extra-header')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
@endsection

@section('title'){{ $group->name }}@endsection

@section('content')
    <section class="center">
        <h1>Voeg een lid toe aan {{ $group->name }}</h1>
        <form method="post" action="{{ route('groups.newmember', $group) }}">
            @csrf
            <label for="users[]">Kies een of meerdere gebruikers</label>
            <select id="users" name="users[]" multiple="multiple">
                @foreach ($candidates as $candidate)
                    <option value="{{ $candidate->id }}">{{ $candidate->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('users[]'))
                <div class="alert error" role="alert">
                    {{ $errors->first('users[]') }}
                </div>
            @endif

            <div class="group">
                <button type="submit"><i class="fa fa-user-plus"></i> Voeg toe</button>
            </div>
        </form>
    </section>
@endsection

@section('scriptarea')
    <script>
        $(document).ready(function() {
            $('#users').select2();
        });
    </script>
@endsection
