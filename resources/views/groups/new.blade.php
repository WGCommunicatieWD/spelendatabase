@extends('layouts.app')

@section('title', 'Nieuwe Groep')

@section('content')
    <section class="center">
        <h1>Nieuwe groep</h1>

        <form method="POST" action="{{ route('groups.store') }}">
            @csrf

            <label for="name">{{ __('Name') }}</label>
            <input id="name" type="text" class="{{ $errors->has('name') ? ' errorinput' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <div class="alert error" role="alert">
                    {{ $errors->first('name') }}
                </div>
            @endif

            <label for="description">Beschrijving</label>
            <textarea style="resize:vertical;" @if($errors->has('description')) class="errorinput" @endif cols="80" rows="5" id="description" name="description">{{ old('description') }}</textarea>

                @if ($errors->has('description'))
                <div class="alert error" role="alert">
                    {{ $errors->first('description') }}
                </div>
                @endif

                <label for="public">Is de groep publiek?</label>
                <input name="public" type="hidden" value="0"/>
                <input name="public" type="checkbox" value="1"/>

                @if ($errors->has('public'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('public') }}
                    </div>
                @endif

                <p>
                    Je kan later leden toevoegen aan je groep.
                </p>

                <div class="group">
                    <button type="submit">
                        <span class="fas fa-save"></span>
                        Opslaan
                    </button>
                </div>
        </form>
    </section>
@endsection
