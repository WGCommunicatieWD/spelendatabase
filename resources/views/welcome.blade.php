@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <section class="center">
        <h1>Kazou Spelendatabase</h1>
        <p>
            Welkom! Er zitten momenteel {{ $gameCount }} spelen in de database.
            @if ($gameCount > 0)
                Genoeg inspiratie dus voor komende zomer!
            @else
                Daar moet je toch echt iets aan doen!
            @endif
        </p>
    </section>
    <section class="center">
        <h2>Je kan kijken naar spelen per categorie of terrein.</h2>
        <p>Of je kan <a href="{{route('spelen.index')}}">alle spelen</a> bekijken.</p>
        <div class="wrapper">
            <div class="col-1">
                <h3>Categorieën</h3>
                <ul class="plain">
                    @foreach ($categorieen as $categorie)
                        <li>
                            <a href="{{ route('categories.show', $categorie) }}">
                                {{ $categorie->naam }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-1">
                <h3>Terreinen</h3>
                <ul class="plain">
                    @foreach ($terreinen as $terrein)
                        <li>
                            <a href="{{ route('terrein', $terrein) }}">
                                {{ $terrein->naam }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>

    <!-- meest recent -->
    <section class="center">
        <h2>Laatst toegevoegde spelen</h2>
        <!-- spelen die het laatste toegevoegd is + link naar spel-->
        <ul class="plain">
            @foreach ($recente as $spel)
                <li>
                    <a href="{{ route('spelen.show', $spel) }}">
                        {{ $spel->titel }}
                    </a>
                </li>
            @endforeach
        </ul>
    </section>

    <section class="center">
        <h2>Hoogste score</h2>
        <!-- spelen die het Meest vind ik luek + link naar spel-->
        <ol>
            @foreach ($hoogsteRank as $spel)
                <li>
                    <a href="{{ route('spelen.show', $spel) }}">
                        {{ $spel->titel }}
                    </a> ({{$spel->score}})
                </li>
            @endforeach
        </ol>
    </section>
@endsection
