@extends('layouts.app')

@section('left-bar')
    @can('manage users')
        <section class="center">
            <h1>Gebruikers</h1>
            <a href="{{ route('users.index') }}">Alle gebruikers</a>
        </section>
    @endcan
@endsection

@section('content')
    @if ($tecontroleren->count() > 0)
        <section class="center">
            <h1>Goed te keuren spelen</h1>
            <ul>
                @foreach ($tecontroleren as $spel)
                    <li>
                        <a href="{{ route('spelen.show', $spel) }}">
                            {{ $spel->titel }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </section>
    @endif

    @if ($reported->count() > 0)
        <section class="center">
            <h1>Gerapporteerde spelen</h1>
            <ul>
                @foreach($reported as $game)
                    <li>
                        <a href="{{ route('spelen.show', $game) }}">
                            {{ $game->titel }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </section>
    @endif

    @if ($verborgen->count() > 0)
        <section class="center">
            <h1>Verborgen spelen</h1>
            <ul>
                @foreach ($verborgen as $spel)
                    <li>
                        <a href="{{ route('spelen.show', $spel) }}">
                            {{ $spel->titel }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </section>
    @endif

    <section class="center">
        <h1>Terreinen</h1>

        <ul>
            @foreach ($terreinen as $terrein)
                <li><a href="{{ route('terrein', $terrein) }}">
                    {{ $terrein->naam }}
                </a></li>
            @endforeach
        </ul>
        <form action="{{ route('terrein.store') }}" method="post">
            @csrf
            <h3><label for="terreinnaam">Nieuw terrein</label></h3>
            <input placeholder="Naam van het nieuwe terrein" type="text" name="naam" id="terreinnaam">
            <div class="group">
                <button type="submit">Opslaan</button>
            </div>
        </form>
    </section>

    <section class="center">
        <h1>Categorieën</h1>
        <ul>
            @foreach ($categorieen as $categorie)
                <li><a href="{{ route('categories.show', $categorie) }}">
                    {{ $categorie->naam }}
                </a></li>
            @endforeach
        </ul>
        <form action="{{ route('categories.store') }}" method="post">
            @csrf
            <h3><label for="naam">Nieuwe categorie</label></h3>
            <input placeholder="Naam van de nieuwe categorie" type="text" name="naam" id="categorienaam">
            <div class="group">
                <button type="submit">Opslaan</button>
            </div>
        </form>
    </section>
@endsection
