<!DOCTYPE html>
<html lang="nl">
    <head>
        <title>Spelendatabase: @yield('title')</title>

        <link rel='shortcut icon' href='{{ asset('images/favicon.ico') }}' type='image/png' />

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <!-- Magic make it faster -->
        <script src="//instant.page/5.1.0" type="module" integrity="sha384-by67kQnR+pyfy8yWP4kPO12fHKRLHZPfEsiSXR8u2IKcTdxD805MGUXBzVPnkLHw"></script>

        @yield('extra-header')
    </head>

    <body>
        <nav class="top">
            <a href="/">
                <img class="brand" alt="Kazou logo" src="/img/LOGO_KAZOU.svg"/>
            </a>
            <label for="nav-hamburger">&#9776;</label>
            <input id="nav-hamburger" type="checkbox"/>
            <div class="toggle-mobile">
                <ul>
                    <li><a href="{{ route('spelen.create') }}"><span class="fa fa-file"></span> Spel Toevoegen</a></li>
                    <li><a href="{{ route('spelen.zoek') }}"><span class="fa fa-search"></span> Spelen Zoeken</a></li>
                    <li><a href="{{ route('groups.index') }}"><span class="fas fa-users"></span> Groepen</a></li>
                    @guest
                    <li><a href="{{ route('login') }}"><span class="fa fa-user"></span> Inloggen</a></li>
                    @endguest
                    @auth
                    <li><a href="{{ route('users.show', \Auth::user()) }}">
                        <span class="fa fa-user"></span> Mijn profiel
                    </a></li>
                    @can('access admin')
                    <li><a href="{{ route('admin') }}">
                        <span class="fa fa-cog"></span> Admin
                    </a></li>
                    @endcan
                    <li>
                        <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <button type="submit">
                                <span class="fa fa-sign-out-alt"></span> {{ __('Logout') }}
                            </button>
                        </form>
                    </li>
                    @endauth
                </ul>
            </div>
        </nav>

        <!-- pagina-->
        <div id="main" class="wrapper">
            <aside>
                @yield('left-bar')
            </aside>
            <main>
                @if (\Session::has('success'))
                    <section class="alert success">
                        <span class="fas fa-check"></span>
                        {{ \Session::get('success') }}
                    </section>
                @endif
                @if (\Session::has('error'))
                    <section class="alert error">
                        <span class="fas fa-times-circle"></span>
                        {{ \Session::get('error') }}
                    </section>
                @endif
                    @auth
                        @if (!Auth::user()->hasVerifiedEmail())
                            <section class="alert warning center">
                                <h2>
                                    <span class="fas fa-exclamation-triangle"></span>
                                    Klik op de link in je e-mail om je account te verifiëren.
                                </h2>
                                E-mail niet ontvangen?
                                <form action="{{ route('verification.resend') }}" method="post">
                                    @csrf
                                    <button type="submit">
                                        <span class="fas fa-envelope"></span>
                                        {{ __('click here to request another') }}
                                    </button>
                                </form>
                            </section>
                        @endif
                    @endauth
                @yield('content')
            </main>
            <aside>
                @yield('right-bar')
            </aside>
        </div>
        <footer>
            <p>
                Een project van en door de vrijwilligers van WG Communicatie, Kazou Waas & Dender.
            </p>
            <p>
                <span class="fab fa-gitlab"></span>&nbsp;<a href="https://gitlab.com/WGCommunicatieWD/spelendatabase/">Broncode</a>
            </p>
            <p>
                Problemen of vragen? <a href="mailto:incoming+wgcommunicatiewd-spelendatabase-9017563-issue-@incoming.gitlab.com">Contacteer ons!</a>
            </p>
        </footer>

        <script src="{{ mix('js/app.js') }}"></script>
        @yield('scriptarea')
    </body>
</html>
