@extends('layouts.app')

@section('title', $gebruiker->name)

    @section('content')
        <section class="center">
            <h1>
                @include('components.gebruiker')
            </h1>
            <p>
                Kazou <a href="{{ route('verbonden.show', $gebruiker->verbond) }}">
                {{ $gebruiker->verbond->name }}
                </a>
            </p>
            @can('manage users')
            <p>
                <a href="mailto://{{ $gebruiker->email }}">{{ $gebruiker->email }}</a>
            </p>
            @endcan
        </section>

        <section class="center">
            <h3>Favorieten</h3>
            <ul>
                @foreach ($gebruiker->favourites as $favourite)
                    @can('discover', $favourite->spel)
                        <li>
                            <a href="{{ route('spelen.show', $favourite->spel) }}">
                                {{ $favourite->spel->titel }}
                            </a>
                        </li>
                    @endcan
                @endforeach
            </ul>
        </section>

        <section class="center">
            <h3>Spelen door {{ $gebruiker->name }}</h3>
            <ul>
                @foreach($gebruiker->spelen as $spel)
                    @can('discover', $spel)
                    <li>
                        <a href="{{ route('spelen.show', $spel) }}">
                            {{$spel->titel}}
                        </a>
                    </li>
                    @endcan
                @endforeach
            </ul>
        </section>

        <section class="center">
            <h3>Groepen</h3>
            <ul>
                @foreach($gebruiker->groups as $group)
                    @can('view', $group)
                    <li>
                        <a href="{{ route('groups.show', $group) }}">
                            {{ $group->name }}
                        </a>
                    </li>
                    @endcan
                @endforeach
            </ul>
        </section>

    @endsection

    @can('manage users')
    @section('left-bar')
        <section class="center">
            @if ($gebruiker->hasRole('moderator'))
                <form method="post" action="{{ route('users.demote', $gebruiker) }}">
                    @csrf
                    @method('PATCH')
                    <button class="dark" type="submit">Maak gewone gebruiker</button>
                </form>
            @else
                <form method="post" action="{{ route('users.promote', $gebruiker) }}">
                    @csrf
                    @method('PATCH')
                    <button type="submit">Promoveer gebruiker</button>
                </form>
            @endif
            @if ($gebruiker->hasRole('verified'))
                <form method="post" action="{{ route('users.unverify', $gebruiker) }}">
                    @csrf
                    @method('PATCH')
                    <button type="submit">Onverifieer gebruiker</button>
                </form>
            @else
                <form method="post" action="{{ route('users.verify', $gebruiker) }}">
                    @csrf
                    @method('PATCH')
                    <button type="submit">Verifieer gebruiker</button>
                </form>
            @endif
        </section>
    @endsection
    @endcan

    @if (\Gate::allows('edit-profile', $gebruiker))
        @section('right-bar')
            <section class="center">
                <a class="btn primary" href="{{ route('users.edit', $gebruiker) }}">
                    <span class="fa fa-pencil-alt"></span> Bewerk profiel
                </a>
            </section>
        @endsection
    @endif
