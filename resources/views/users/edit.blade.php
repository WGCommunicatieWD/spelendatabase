@extends('layouts.app')

@section('content')
    <section class="center">

        <h1>Wijzig je profiel</h1>

        <form method="POST" action="{{ route('users.update', $user) }}">
            @csrf
            @method('PUT')

            <label for="name">{{ __('Name') }}</label>
            <input id="name" type="text" class="{{ $errors->has('name') ? ' errorinput' : '' }}" name="name" value="{{ old('name', $user->name) }}" required autofocus>

            @if ($errors->has('name'))
                <div class="alert error" role="alert">
                    {{ $errors->first('name') }}
                </div>
            @endif

            <label for="email">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="{{ $errors->has('email') ? ' errorinput' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>

            @if ($errors->has('email'))
                <div class="alert error" role="alert">
                    {{ $errors->first('email') }}
                </div>
            @endif

            <label for="verbond_id">Verbond</label>
            <select id="verbond_id" name="verbond_id">
                @foreach (\App\Verbond::all() as $verbond)
                    <option value="{{ $verbond->id }}" @if ($verbond->id == $user->verbond->id) selected @endif>
                        {{ $verbond->name }}
                    </option>
                @endforeach
            </select>

            @if ($errors->has('verbond_id'))
                <div class="alert error" role="alert">
                    {{ $errors->first('verbond_id') }}
                </div>
            @endif

            <label for="password">Nieuw Wachtwoord (leeg laten om ongewijzigd te laten)</label>
            <input id="password" type="password" class="{{ $errors->has('password') ? ' errorinput' : '' }}" name="password">

            @if ($errors->has('password'))
                <div class="alert error" role="alert">
                    {{ $errors->first('password') }}
                </div>
            @endif

            <label for="password-confirm">Bevestig nieuw paswoord</label>
            <input id="password-confirm" type="password" name="password_confirmation">

            <label for="currentpassword">Huidig wachtwoord</label>
            <input name="currentpassword" id="currentpassword" class="{{ $errors->has('currentpassword') ? 'errorinput' : '' }}" type="password" required/>

            @if ($errors->has('currentpassword'))
                <div class="alert error" role="alert">
                    {{ $errors->first('currentpassword') }}
                </span>
            @endif

            <div class="group">
                <button type="submit">
                    <span class="fas fa-save"></span>
                    {{ __('Save') }}
                </button>
            </div>
        </form>

    </section>
@endsection
