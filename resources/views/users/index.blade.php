@extends('layouts.app')

@section('title', 'Alle gebruikers')

@section('content')
    <section class="center">
        <h1>Alle gebruikers</h1>
        <table class="table">
            <thead>
                <th scope="col">Naam</th>
                <th scope="col">Verbond</th>
                <th scope="col">Aantal spelen</th>
                <th scope="col">E-mail geverifieerd</th>
                <th scope="col">E-mailadres</th>
            </thead>
            <tbody>
                @foreach($gebruikers as $gebruiker)
                    <tr>
                        <th scope="row">
                            @include('components.gebruiker')
                        </th>
                        <td>{{ $gebruiker->verbond->name }}</td>
                        <td>{{ $gebruiker->spelen()->count() }}</td>
                        <td>
                            @if (is_null($gebruiker->email_verified_at))
                                <span class="fas fa-times-circle"></span>
                            @else
                                <span class="far fa-check-circle"></span>
                            @endif
                        </td>
                        <td>
                            <a href="mailto:{{ $gebruiker->email }}">
                                {{ $gebruiker->email }}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </section>
@endsection
