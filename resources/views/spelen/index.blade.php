@extends('layouts.app')

@section('title', 'Alle spelen')

@section('content')

    <section class="center">
        <h1>Alle spelen</h1>
        @if ($spelen->count() < 1)
            <p>
                Er zijn nog geen spelen toegevoegd...
            </p>
        @endif
    </section>

    @component('components.shortspelen', ['spelen' => $spelen])
    @endcomponent

@endsection

@section('right-bar')
    <section class="center">
        <h3>Alle Categorieën</h3>
        <ul class="plain">
            @foreach ($categorieen as $cat)
                <li>
                    <a href="{{ route('categories.show', $cat) }}">
                        {{ $cat->naam }}
                    </a>
                </li>
            @endforeach
        </ul>
    </section>

    <section class="center">
        <h3>Alle Terreinen</h3>
        <ul class="plain">
            @foreach ($terreinen as $cat)
                <li><a href="{{ route('terrein', $cat) }}">
                    {{ $cat->naam }}
                </a></li>
            @endforeach
        </ul>
    </section>
@endsection
