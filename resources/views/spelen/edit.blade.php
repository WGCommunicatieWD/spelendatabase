@extends('layouts.app')

@section('title', 'Nieuw Spel')

@section('extra-header')
    <!-- EasyMDE -->
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
@endsection

@section('content')
    <section class="center">
        <h1>Bewerk Spel "{{ $spel->titel }}"</h1>
        <h3>TIA UND TIM</h3>
        <p>
            <b>T</b>errein, <b>I</b>nkleding, <b>A</b>antal deelnemers, <b>U</b>itleg, <b>N</b>aam, <b>D</b>uur, <b>T</b>aakverdeling, <b>I</b>nclusie, <b>M</b>ateriaal
        </p>
    </section>

    @if ($errors->any())
        <section class="alert error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </section>
    @endif

    <form action="{{ route('spelen.show', ['spel' => $spel]) }}" method="post">
        @csrf
        @method('PATCH')

        <section class="center">
            <h2>Titel</h2>
            <p>Dit is verplicht.</p>
            <input type="text" name="titel" id="titel" value="{{$spel->titel}}" required>
        </section>

        <section class="center">
            <h2>Terrein</h2>
            <select name="terreinId" id="terrein">
                @foreach ($terreinen as $terrein)
                    <option @if($terrein->id == $spel->terreinId) selected @endif value="{{ $terrein->id }}">
                        {{ $terrein->naam }}
                    </option>
                @endforeach
            </select>
        </section>

        <section class="center">
            <h2>Categorie</h2>
            <select name="categorieId" id="categorie">
                @foreach ($categorieen as $categorie)
                    <option @if($categorie->id == $spel->categorieId) selected @endif value="{{ $categorie->id }}">
                        {{ $categorie->naam }}
                    </option>
                @endforeach
            </select>
        </section>

        <section class="center">
            <h2>Inkleding</h2>
            <textarea name="inkleding" id="inkleding" cols="80" rows="20">{{ $spel->inkleding }}</textarea>
        </section>

        <section class="center">
            <h2>Aantal deelnemers</h2>
            <p>Een schatting is prima hier.</p>
            <p>Dit is verplicht.</p>
            <input name="deelnemers" type="number" min="0" max="99" value="{{$spel->deelnemers}}" required>
            <h2>Leeftijd deelnemers</h2>
            <p>Dit is verplicht.</p>
            <input name="is_jomba" type="hidden" value="0"/>
            <input name="is_jomba" id="jomba" type="checkbox" value="1" @if(old('is_jomba', $spel->is_jomba)) checked @endif />
            <label for="jomba">JOMBA</label>
            <div class="wrapper">
                <div class="col-1">
                    <label for="minLeeftijd">Minimum leeftijd</label>
                    <input type="number" name="minLeeftijd" id="minLeeftijd" min="0" max="99" value="{{$spel->minLeeftijd}}" required>
                </div>
                <div class="col-1">
                    <label for="maxLeeftijd">Maximum leeftijd</label>
                    <input type="number" name="maxLeeftijd" id="maxLeeftijd" min="0" max="99" value="{{$spel->maxLeeftijd}}" required>
                </div>
            </div>
            <h2>Aantal moni's</h2>
            <p>Ongeveer is goed genoeg.</p>
            <input type="number" name="monis" min="0" max="99" value="{{$spel->monis}}">
        </section>

        <section class="center">
            <h2>Uitleg</h2>
            <p>Dit is verplicht.</p>
            <textarea name="uitleg" id="uitleg" cols="80" rows="50" required>{{ $spel->uitleg }}</textarea>
        </section>

        <section class="center">
            <h2>Duur</h2>
            <p>In minuten, je mag gokken.</p>
            <p>Dit is verplicht.</p>
            <input type="number" name="duurMinuten" id="duurMinuten" min="0" max="65535" value="{{$spel->duurMinuten}}" required>
        </section>

        <section class="center">
            <h2>Taakverdeling</h2>
            <textarea name="taakverdeling" id="taakverdeling" cols="80" rows="20">{{ $spel->taakverdeling }}</textarea>
        </section>

        <section class="center">
            <h2>Inclusie</h2>
            <textarea name="inclusie" id="inclusie" cols="80" rows="20">{{ $spel->inclusie }}</textarea>
        </section>

        <section class="center">
            <h2>Materiaal</h2>
            <textarea name="materiaal" id="materiaal" cols="80" rows="20">{{ $spel->materiaal }}</textarea>
        </section>

        <section class="center">
            <h2>Klaar om te verzenden?</h2>
            <input type="hidden" name="isDraft" value="0">
            <input name="isDraft" id="isDraft" type="checkbox" value="1" @if($spel->isDraft) checked @endif /><label> Opslaan als klad</label>
            <div class="group">
                <button class="primary" save type="submit">
                    <span class="fa fa-save"></span>
                    Opslaan
                </button>
            </div>
        </section>
    </form>

@endsection

@section('scriptarea')
    @include('components.confirm-exit')
    <script>
        function genMDEConfig(id) {
            return {
                autosave: {
                    enabled: false, // Disabled here because it might apply unwanted changes.
                    delay: 5000,
                    uniqueId: id + "Edit",
                },
                promptURLs: true, forceSync: true, spellChecker: false, element: document.getElementById(id),
            };
        };

        var inkledingMDE = new EasyMDE(genMDEConfig("inkleding"));
        var uitlegMDE = new EasyMDE(genMDEConfig("uitleg"));
        var taakverdelingMDE = new EasyMDE(genMDEConfig("taakverdeling"));
        var inclusieMDE = new EasyMDE(genMDEConfig("inclusie"));
        var materiaalMDE = new EasyMDE(genMDEConfig("materiaal"));
    </script>
@endsection
