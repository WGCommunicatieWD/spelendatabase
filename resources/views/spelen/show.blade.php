@extends('layouts.app')

@section('extra-header')
    <!-- EasyMDE -->
    <link rel="stylesheet" href="https://unpkg.com/easymde@2.10.1/dist/easymde.min.css">
    <script src="https://unpkg.com/easymde@2.10.1/dist/easymde.min.js"></script>
@endsection

@section('title', $spel->titel)

    @section('left-bar')
        <section class="center">
            <a target="_blank" href="{{ route('spelen.print', $spel) }}" class="btn primary">
                <span class="fa fa-print"></span> Print
            </a>
            <a href="{{ route('spelen.export', $spel) }}" class="btn primary">
                <span class="fa fa-file-word"></span> Download
            </a>
            @auth
            <form class="d-inline" action="{{ route('spelen.copy', $spel) }}" method="post">
                @csrf
                <button class="primary" type="submit">
                    <span class="fas fa-copy"></span> Kopieer
                </button>
            </form>
            <a class="btn primary" href="{{ route('spelen.attachments', $spel) }}">
                <span class="fas fa-file"></span>
                Bijlagen
            </a>
            <a class="btn primary" href="{{ route('spelen.addtogroup', $spel) }}">
                <span class="fa fa-user-plus"></span> Voeg toe aan groep
            </a>
        </section>
        <section class="center">
            @endauth
            @can('approve games')
                @if (!$spel->zichtbaar)
                    <form action="{{ route('spelen.approve', $spel) }}" method="post" class="d-inline">
                        @csrf
                        @method('PATCH')
                        <button class="primary" type="submit">
                            <span class="fas fa-thumbs-up"></span>
                            Keur goed
                        </button>
                    </form>
                @else
                    <form action="{{ route('spelen.deny', $spel) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <button class="dark" type="submit">
                            <span class="fas fa-thumbs-down"></span>
                            Keur af
                        </button>
                    </form>
                @endif
                @if ($spel->flagged)
                        <form action="{{ route('spelen.clear-flags', $spel) }}" method="post" class="d-inline">
                            @csrf
                            <button class="primary" type="submit">
                                <span class="far fa-flag"></span>
                                Verwijder flags
                            </button>
                        </form>
                @endif
            @endcan
            @can('manage-spel', $spel)
                <form action="{{ route('spelen.show', $spel) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="danger">
                        <span class="fas fa-trash"></span> Verwijder
                    </button>
                </form>
            @endcan
        </section>
    @endsection

    @section('content')
        <section class="center">
            <div class="wrapper">
                <div class="col-1">
                </div>
                <div class="col-2">
                    @include('components.game-badges')
                    <h1>
                        {{ $spel->titel }} @can('manage-spel', $spel)<a href="{{route('spelen.edit', ['spel' => $spel])}}"><i class="fa fa-pencil-alt"></i></a>@endcan
                    </h1>
                        @if($spel->isDraft) <h2>Klad</h2> @endif
                    @unless(is_null($spel->original))
                        @can('view', $spel->original)
                        <p>
                            Gebaseerd op <a href="{{ route('spelen.show', $spel->original) }}">{{ $spel->original->titel }}</a>
                        </p>
                        @endcan
                    @endunless

                    @unless (is_null($spel->maker))
                        <p>
                            <em>
                                Door @include('components.gebruiker', ['gebruiker' => $spel->user])
                            </em>
                        </p>
                    @endunless
                    <p>{{ $spel->views }} keer bekeken</p>
                </div>
                <div class="col-1">

                    @can('stem-spel', $spel)
                    @if ($loves)
                        <h2>
                            <form action="{{ route('spelen.love', $spel) }}" method="post">
                                @csrf
                                <button class="hart">
                                    {{ $spel->aantalStemmen }}
                                </button>
                            </form>
                        </h2>
                    @else
                        <h2>
                            <form action="{{ route('spelen.love', $spel) }}" method="post">
                                @csrf
                                <button class="hart-off">
                                    {{ $spel->aantalStemmen }}
                                </button>
                            </form>
                        </h2>
                    @endif
                    @endcan

                    @cannot('stem-spel', $spel)
                        <h2>
                            @csrf
                            <a class="hart-off hartoff">
                                {{ $spel->aantalStemmen }}
                            </a>
                        </h2>
                    @endcannot

                </div>
            </div>
        </section>



        @if (!is_null($spel->inkleding))
            <section class="center">
                <h2>Inkleding</h2>
                <div class="user-html">
                    @markdown($spel->inkleding)
                </div>
            </section>
        @endif

        <section class="center">
            <h2>Uitleg</h2>
            <div class="user-html">
                @markdown($spel->uitleg)
            </div>
        </section>

        @if (!is_null($spel->taakverdeling))
            <section class="center">
                <h2>Taakverdeling</h2>
                <div class="user-html">
                    @markdown($spel->taakverdeling)
                </div>
            </section>
        @endif

        @if (!is_null($spel->inclusie))
            <section class="center">
                <h2>Inclusie</h2>
                <div class="user-html">
                    @markdown($spel->inclusie)
                </div>
            </section>
        @endif

        @if (!is_null($spel->materiaal))
            <section class="center">
                <h2>Materiaal</h2>
                <div class="user-html">
                    @markdown($spel->materiaal)
                </div>
            </section>
        @endif

        @if ($spel->attachments->count() > 0)
            <section class="center">
                <h2>Bijlagen</h2>
                <ul>
                    @foreach ($spel->attachments as $a)
                        <li><a href="{{ route('spelen.attachments.download', ['spel'=>$spel,'attachment'=>$a]) }}">{{ $a->originalname }}</a></li>
                    @endforeach
                </ul>
            </section>
        @endif

        <hr>

        @foreach ($spel->comments as $comment)
            @can('view', $comment)
                <section class="inverted">
                    <h3>
                        {{ $comment->user->name }} <small>{{ $comment->created_at->shortRelativeDiffForHumans() }}</small>
                        @if ($comment->restricted_to_author)
                            <span class="fa fa-eye-slash" title="Enkel zichtbaar voor de auteur van dit spel."></span>
                        @endif
                        @if ($comment->flag && is_null($comment->flag_removed_at))
                            <span class="fas fa-flag" title="Gerapporteerd aan moderatoren"></span>
                        @elseif ($comment->flag && $comment->flag_removed_at != null)
                            <span class="far fa-flag" title="Gerapporteerd en gecontroleerd"></span>
                        @endif
                    </h3>
                    <div class="user-html">
                        @markdown($comment->content)
                    </div>
                </section>
            @endcan
        @endforeach

        <section class="inverted">
            <h2>Nieuwe reactie</h2>
            <form action="{{ route('spelen.comments.store', $spel) }}" method="post">
                @csrf
                <textarea name="content" id="comment-content" cols="30" rows="10"></textarea>
                <div class="group">
                    <input type="hidden" name="restricted_to_author" value="0">
                    <input type="checkbox" name="restricted_to_author" id="restricted_to_author" value="1">
                    <label for="restricted_to_author">Enkel zichtbaar voor de auteur?</label>
                </div>
                <div class="group">
                    <input type="hidden" name="flag" value="0">
                    <input type="checkbox" name="flag" id="flag" value="1">
                    <label for="flag">Rapporteer naar moderatoren?</label>
                </div>
                <div class="group">
                    <button class="primary" type="submit">
                        <span class="fa fa-save"></span>
                        Opslaan
                    </button>
                </div>
                <div class="group">
                    <i>Je kan dit niet meer bewerken later.</i>
                </div>
            </form>
        </section>

    @endsection

    @section('right-bar')
        <section class="center">
            <div class="wrapper">
                <div class="col-1">
                    @if (!is_null($spel->categorieId))
                        <h2>Categorie</h2>
                        <p><a href="{{route('categories.show', ['category'=>$spel->categorie])}}">
                            {{ $spel->categorie->naam }}</a></p>
                    @endif

                    <h2>Leeftijd</h2>
                    @if ($spel->is_jomba)
                        <p>JOMBA ({{ $spel->minLeeftijd }} - {{ $spel->maxLeeftijd }})</p>
                    @else
                        <p>Van {{ $spel->minLeeftijd }} tot {{ $spel->maxLeeftijd }} jaar.</p>
                    @endif

                    <h2>Aantal deelnemers</h2>

                    <p>Ongeveer {{ $spel->deelnemers }}</p>

                    <h2>Aantal moni's</h2>
                    @if($spel->monis == 1)
                        <p>
                            {{ $spel->monis }} moni
                        </p>
                    @else
                        <p>
                            {{ $spel->monis }} moni's
                        </p>
                    @endif

                    <h2>Duur</h2>
                    <p>{{ $spel->duurMinuten }} minuten</p>

                    @if (!is_null($spel->terreinId))
                        <h2>Terrein</h2>
                        <p><a href="{{ route('terrein', ['terrein' => $spel->terrein]) }}">{{ $spel->terrein->naam }}</a></p>
                    @endif

                </div>
            </div>
        </section>

        @unless ($spel->derivatives()->count() < 1)
            <section class="center">
                <h2>Afgeleide spelen</h2>
                <ul>
                    @foreach ($spel->derivatives as $derivative)
                        @can ('view', $derivative)
                        <li>
                            <a href="{{ route('spelen.show', $derivative) }}">
                                {{ $derivative->titel }}
                            </a>
                            @unless (is_null($derivative->user))
                                door @include('components.gebruiker', ['gebruiker' => $derivative->user])
                            @endunless
                        </li>
                        @endcan
                    @endforeach
                </ul>
            </section>
        @endunless
    @endsection
@section('scriptarea')
    <script>
        function genMDEConfig(id) {
            return {
                autosave: {
                    enabled: true,
                    delay: 5000,
                    uniqueId: id + "New",
                },
                promptURLs: true, forceSync: true, spellChecker: false, element: document.getElementById(id),
            };
        };

        var mde = new EasyMDE(genMDEConfig("comment-content"));
    </script>
@endsection
