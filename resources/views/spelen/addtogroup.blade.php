@extends('layouts.app')

@section('extra-header')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endsection

@section('title'){{ $spel->titel }}@endsection

@section('content')
    <section class="center">
        <h1>Voeg dit spel toe aan een groep</h1>
        <form method="post" action="{{ route('spelen.addtogroup', ['spel' => $spel]) }}">
            @csrf
            <label for="group">Kies een groep</label>
            <select id="group" name="group">
                @foreach ($groups as $group)
                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('group'))
                <div class="alert error" role="alert">
                    {{ $errors->first('group') }}
                </div>
            @endif

            <label for="comment">Opmerking (optioneel)</label>
            <textarea style="resize:vertical;" cols="5" id="comment" name="comment" rows="5">{{ old('comment') }}</textarea>

            @if ($errors->has('comment'))
                <div class="alert error" role="alert">
                    {{ $errors->first('comment') }}
                </div>
            @endif

            <label for="scheduled">Wanneer is dit spel gepland? (optioneel)</label>
            <input id="scheduled" class="flatpickr flatpickr-input" name="scheduled" type="text" value="{{ old('scheduled') }}" readonly="readonly" />
            <label for="doschedule">Plan het spel?</label>
            <input name="doschedule" type="hidden" value="0"/>
            <input id="doschedule" name="doschedule" type="checkbox" value="1" />

            @if ($errors->has('scheduled'))
                <div class="alert error" role="alert">
                    {{ $errors->first('scheduled') }}
                </div>
            @endif

            <div class="group">
                <button type="submit">
                    <span class="fa fa-user-plus"></span> Voeg toe
                </button>
            </div>
        </form>
    </section>
@endsection

@section('scriptarea')
    <script>
        $(document).ready(function() {
            $('#group').select2();
            $('#scheduled').flatpickr({
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                time_24hr: true,
            });
        });
    </script>
@endsection
