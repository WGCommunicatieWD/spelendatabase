@extends('layouts.app')

@section('title', $spel->titel)

    @section('content')
        <section class="center">
            <h1>Bijlagen bij {{ $spel->titel }}</h1>
            <ul>
                @foreach ($spel->attachments as $a)
                    <li>
                        <a href="{{ route('spelen.attachments.download', ['spel'=>$spel,'attachment'=>$a]) }}">{{ $a->originalname }}</a>
                        @can('manage-spel', $spel)
                        <form action="{{ route('spelen.attachments.delete', ['spel' => $spel, 'attachment' =>$a]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="danger" type="submit">
                                <span class="fas fa-trash-alt"></span>
                            </button>
                        </form>
                        @endcan
                    </li>
                @endforeach
            </ul>
        </section>

        @can('manage-spel', $spel)
        <section class="center">
            <h2>Nieuwe bijlagen</h2>
            <form action="{{ route('spelen.attachments', ['spel' => $spel]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="attachments">Kies één of meerdere bijlagen (foto's, documenten, video en audio toegelaten, max. 2 MiB)</label>
                <input name="attachments[]" id="attachments" type="file" multiple="multiple" />
                @if ($errors)
                    @foreach($errors->all() as $error)
                        <div class="alert error" role="alert">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                <div class="group">
                    <button type="submit">
                        <span class="fa fa-save"></span> Opslaan
                    </button>
                </div>
            </form>
        </section>
        @endcan
    @endsection
