<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>{{ $spel->titel }}</title>
    <link rel="stylesheet" href="{{ asset('css/print.css') }}">
</head>
<body>
<div id="container">
    <h1>{{ $spel->titel }}</h1>

    @unless(is_null($spel->user))
        <em>Door {{ $spel->user->name }}</em>
    @endunless

    <h3>Aantal deelnemers</h3>
    <p>Ongeveer {{ $spel->deelnemers }}</p>

    <h3>Aantal moni's</h3>
    <p>{{ $spel->monis }} moni's</p>

    <h3>Duur</h3>
    <p>{{ $spel->duurMinuten }} minuten</p>

    @if (!is_null($spel->terreinId))
        <h3>Terrein</h3>
        <p>{{ $spel->terrein->naam }}</p>
    @endif

    @if (!is_null($spel->inkleding))
        <h3>Inkleding</h3>
        <div class="user-html">
            @markdown($spel->inkleding)
        </div>
    @endif

    <h3>Uitleg</h3>
    <div class="user-html">
        @markdown($spel->uitleg)
    </div>

    @if (!is_null($spel->taakverdeling))
        <h3>Taakverdeling</h3>
        <div class="user-html">
            @markdown($spel->taakverdeling)
        </div>
    @endif

    @if (!is_null($spel->inclusie))
        <h3>Inclusie</h3>
        <div class="user-html">
            @markdown($spel->inclusie)
        </div>
    @endif

    @if (!is_null($spel->materiaal))
        <h3>Materiaal</h3>
        <div class="user-html">
            @markdown($spel->materiaal)
        </div>
    @endif
</div>
</body>
</html>
