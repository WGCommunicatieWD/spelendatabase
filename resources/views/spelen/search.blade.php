@extends('layouts.app')

@section('title', 'Zoeken')

@section('content')
    <section class="center">
        <h1>Zoek een spel</h1>
        @if ($errors->any())
            <section class="alert error">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </section>
        @endif
        <form action="{{ route('spelen.zoek') }}" method="get">
            <div class="wrapper">
                <label for="categorie" class="col">Categorie</label>
                <div class="col-2">
                    <select name="categorie" id="categorie">
                        <option value selected>-- Kies een categorie --</option>
                        @foreach ($categorieen as $cat)
                            <option @if (old('categorie') == $cat->id) selected @endif value="{{ $cat->id }}">
                                {{ $cat->naam }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="wrapper">
                <label for="terrein" class="col">Terrein</label>
                <div class="col-2">
                    <select name="terrein" id="terrein">
                        <option value selected>-- Kies een terrein --</option>
                        @foreach ($terreinen as $ter)
                            <option @if (old('terrein') == $ter->id) selected @endif value="{{ $ter->id }}">
                                {{ $ter->naam }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="wrapper">
                <label class="col">Leeftijd</label>
                <div class="col-2">
                    <input type="number" name="minLeeftijd" id="minLeeftijd" min="0" max="99" placeholder="Van" value="{{ old('minLeeftijd') }}">
                    <input type="number" name="maxLeeftijd" id="maxLeeftijd" min="0" max="99" placeholder="Tot" value="{{ old('maxLeeftijd') }}">
                </div>
            </div>
            <div class="wrapper">
                <label class="col" for="">JOMBA</label>
                <div class="col-2">
                    <select id="is_jomba" name="is_jomba">
                        <option value="" @if(old('is_jomba') == null || old('is_jomba') == "null" ) selected @endif >
                            Alles
                        </option>
                        <option value="1" @if(old('is_jomba') === true || old('is_jomba') == "1" ) selected @endif>
                            Enkel JOMBA
                        </option>
                        <option value="0" @if(old('is_jomba') === false || old('is_jomba') == "0") selected @endif>
                            Geen JOMBA
                        </option>
                    </select>
                </div>
            </div>
            <div class="wrapper">
                <label for="verbond_id" class="col">Verbond</label>
                <div class="col-2">
                    <select id="verbond_id" name="verbond_id">
                        <option value selected>Alle verbonden</option>
                        @foreach (\App\Verbond::all() as $v)
                            <option value="{{ $v->id }}" @if (old('verbond_id') == $v->id) selected @endif >
                                {{ $v->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="group">
                <button type="submit" class="primary">
                    <span class="fa fa-search"></span> Zoek
                </button>
            </div>
        </form>
    </section>

    @component('components.shortspelen', ['spelen' => $spelen])
    @endcomponent

@endsection
