@extends('layouts.app')

@section('title', 'Nieuw Spel')

@section('extra-header')
    <!-- EasyMDE -->
    <link rel="stylesheet" href="https://unpkg.com/easymde@2.10.1/dist/easymde.min.css">
    <script src="https://unpkg.com/easymde@2.10.1/dist/easymde.min.js"></script>
@endsection

@section('content')
    <section class="center">
        <h1>Nieuw Spel</h1>
        <p>
            Als je bent ingelogd, kan je achteraf je spel nog bewerken.
            Anders wordt dit spel opgeslagen als een anoniem spel.
        </p>
        <h3>TIA UND TIM</h3>
        <p>
            <b>T</b>errein, <b>I</b>nkleding, <b>A</b>antal deelnemers, <b>U</b>itleg, <b>N</b>aam, <b>D</b>uur, <b>T</b>aakverdeling, <b>I</b>nclusie, <b>M</b>ateriaal
        </p>
    </section>

    @if ($errors->any())
        <section class="alert error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </section>
    @endif

    <form action="{{ route('spelen.index') }}" method="post">
        @csrf
        <section class="center">
            <h2>Titel</h2>
            <p>Dit is verplicht.</p>
            <input type="text" name="titel" id="titel" required value="{{ old('titel') }}">
        </section>

        <section class="center">
            <h2>Terrein</h2>
            <select name="terreinId" id="terrein">
                @foreach ($terreinen as $terrein)
                    <option value="{{ $terrein->id }}" @if($terrein->id == old('terreinId')) selected @endif>
                        {{ $terrein->naam }}
                    </option>
                @endforeach
            </select>
        </section>

        <section class="center">
            <h2>Categorie</h2>
            <select name="categorieId" id="categorie">
                @foreach ($categorieen as $categorie)
                    <option value="{{ $categorie->id }}" @if($categorie->id == old('categorieId')) selected @endif>
                        {{ $categorie->naam }}
                    </option>
                @endforeach
            </select>
        </section>

        <section class="center">
            <h2>Inkleding</h2>
            <textarea name="inkleding" id="inkleding" cols="80" rows="20">{{ old('inkleding') }}</textarea>
            <i>
                <a href="https://gitlab.com/WGCommunicatieWD/spelendatabase/wikis/gebruik/markdown" target="_blank">Help over opmaak</a>
            </i>
        </section>

        <section class="center">
            <h2>Aantal deelnemers</h2>
            <p>Een schatting is prima hier.</p>
            <p>Dit is verplicht.</p>
            <p>
                <input name="deelnemers" type="number" min="0" max="99" required value="{{ old('deelnemers') }}">
            </p>
            <h2>Leeftijd deelnemers</h2>
            <p>Dit is verplicht.</p>
            <input name="is_jomba" id="jomba" type="checkbox" value="1"/>
            <label for="jomba">JOMBA</label>
            <div class="wrapper">
                <div class="col-1">
                    <label for="minLeeftijd">Van</label>
                </div>
                <div class="col-4">
                    <input type="number" name="minLeeftijd" id="minLeeftijd" min="0" max="99" value="{{ old('minLeeftijd', 0) }}" required>
                </div>
                <div class="col-1">
                    <label for="maxLeeftijd">tot</label>
                </div>
                <div class="col-4">
                    <input type="number" name="maxLeeftijd" id="maxLeeftijd" min="0" max="99" value="{{ old('maxLeeftijd', 99) }}" required>
                </div>
                <div class="col-1">
                    jaar.
                </div>
            </div>
            <h2>Aantal moni's</h2>
            <p>Ongeveer is goed genoeg.</p>
            <input type="number" name="monis" min="0" max="99" value="{{ old('monis') }}">
        </section>

        <section class="center">
            <h2>Uitleg</h2>
            <p>Dit is verplicht.</p>
            <textarea name="uitleg" id="uitleg" cols="80" rows="50">{{ old('uitleg') }}</textarea>
            <i>
                <a href="https://gitlab.com/WGCommunicatieWD/spelendatabase/wikis/gebruik/markdown" target="_blank">Help over opmaak</a>
            </i>
        </section>

        <section class="center">
            <h2>Duur</h2>
            <p>In minuten, je mag gokken.</p>
            <p>Dit is verplicht.</p>
            <input type="number" name="duurMinuten" id="duurMinuten" min="0" max="65535" value="{{ old('duurMinuten', 90) }}" required>
        </section>

        <section class="center">
            <h2>Taakverdeling</h2>
            <textarea name="taakverdeling" id="taakverdeling" cols="80" rows="20">{{ old('taakverdeling') }}</textarea>
            <i>
                <a href="https://gitlab.com/WGCommunicatieWD/spelendatabase/wikis/gebruik/markdown" target="_blank">Help over opmaak</a>
            </i>
        </section>

        <section class="center">
            <h2>Inclusie</h2>
            <textarea name="inclusie" id="inclusie" cols="80" rows="20">{{ old('inclusie') }}</textarea>
            <i>
                <a href="https://gitlab.com/WGCommunicatieWD/spelendatabase/wikis/gebruik/markdown" target="_blank">Help over opmaak</a>
            </i>
        </section>

        <section class="center">
            <h2>Materiaal</h2>
            <textarea name="materiaal" id="materiaal" cols="80" rows="20">{{ old('materiaal') }}</textarea>
            <i>
                <a href="https://gitlab.com/WGCommunicatieWD/spelendatabase/wikis/gebruik/markdown" target="_blank">Help over opmaak</a>
            </i>
        </section>

        <section class="center">
            <h2>Klaar om te verzenden?</h2>
            <input name="isDraft" id="isDraft" type="checkbox" value="1" @if(old('isDraft')) checked @endif />
            <label for="isDraft"> Opslaan als klad</label>
            <div class="group">
                <button class="primary" save type="submit">
                    <span class="fa fa-save"></span>
                    Opslaan
                </button>
            </div>
        </section>
    </form>

@endsection

@section('scriptarea')
    <script>
        function genMDEConfig(id) {
            return {
                autosave: {
                    enabled: true,
                    delay: 5000,
                    uniqueId: id + "New",
                },
                promptURLs: true, forceSync: true, spellChecker: false, element: document.getElementById(id),
            };
        };

        var inkledingMDE = new EasyMDE(genMDEConfig("inkleding"));
        var uitlegMDE = new EasyMDE(genMDEConfig("uitleg"));
        var taakverdelingMDE = new EasyMDE(genMDEConfig("taakverdeling"));
        var inclusieMDE = new EasyMDE(genMDEConfig("inclusie"));
        var materiaalMDE = new EasyMDE(genMDEConfig("materiaal"));
    </script>
@endsection
