@foreach ($groups as $group)
    <section class="center">
        <h3><a href="{{ route('groups.show', ['group' => $group]) }}">{{ $group->name }}</a></h3>
        <p>
            {{ $group->description }}
        </p>
    </section>

@endforeach

@if ($groups->count() < 1)
    <section class="center">
        <h2>Geen resultaten.</h2>
    </section>
@endif

{{ $groups->links() }}
