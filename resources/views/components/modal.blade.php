<div>
    <label for="{{ $id }}" {{ $attributes->merge(['class' => 'modal-toggle']) }}>
        {{ $label }}
    </label>
    <input id="{{ $id }}" type="checkbox" class="modal-toggle"/>
    <div class="modal">
        <div class="modal-content">
            <div class="modal-title">
                <label for="{{ $id }}" class="btn danger transparent close inline">&times;</label>
                <h2>{{ $title }}</h2>
            </div>
            {{ $slot }}
        </div>
    </div>
</div>
