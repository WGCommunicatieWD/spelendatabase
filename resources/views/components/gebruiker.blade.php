<a href="{{ route('users.show', ['user' => $gebruiker]) }}">{{ $gebruiker->name }}</a>
@if($gebruiker->hasRole(['admin', 'moderator']))
    <span class="fa fa-shield-alt"></span>
@elseif($gebruiker->can('bypass approval'))
    <span class="fa fa-check"></span>
@endif
