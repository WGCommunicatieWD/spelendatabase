<div class="game-badges">
    @if ($spel->isDraft)
        <span class="fab fa-3x fa-firstdraft" title="Klad"></span>
    @endif
    @if ($spel->is_jomba)
        <span class="fab fa-3x fa-accessible-icon" title="JOMBA"></span>
    @endif
</div>
