<script>
    var unloadListener = function (e) {
        // Cancel the event
        e.preventDefault();
        // Chrome requires returnValue to be set
        e.returnValue = '';
    };
    window.addEventListener('beforeunload', unloadListener);
    document.querySelectorAll("button[type=submit][save]").forEach(function(button) {
        button.addEventListener('click', function() {
            window.removeEventListener('beforeunload', unloadListener);
        });
    });
</script>
