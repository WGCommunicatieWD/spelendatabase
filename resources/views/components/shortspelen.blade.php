@foreach ($spelen as $spel)
    <section class="center">
        @include('components.game-badges')
        <h3>
            <a href="{{route('spelen.show', ['spel' => $spel])}}">{{ $spel->titel }}</a>
        </h3>
        @unless (is_null($spel->maker))
            <p>
                <em>Door @include('components.gebruiker', ['gebruiker' => $spel->user])</em>
            </p>
        @endunless
        <p>
            {{ $spel->duurMinuten }} minuten, {{ $spel->deelnemers }} deelnemers
        </p>
        <p><a href="{{ route('spelen.show', ['spel'=> $spel]) }}">Lees meer...</a></p>
    </section>

@endforeach

@if ($spelen->count() < 1)
    <section class="center">
        <h2>Geen resultaten.</h2>
    </section>
@endif

{{ $spelen->links() }}
