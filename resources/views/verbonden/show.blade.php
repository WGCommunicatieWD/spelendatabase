@extends('layouts.app')

@section('title', $verbond->name)

@section('content')
    <section class="center">
        <h1>Kazou {{ $verbond->name }}</h1>
        <ul>
            @foreach($verbond->users as $gebruiker)
                <li>@include('components.gebruiker')</li>
            @endforeach
        </ul>
    </section>
@endsection

@section('left-bar')
    <section class="center">
        <a class="btn primary" href="{{ route('verbonden.index') }}">
            Terug naar alle verbonden
        </a>
    </section>
@endsection
