@extends('layouts.app')

@section('title', 'Alle verbonden')

@section('content')
    <section class="center">
        <h1>Alle verbonden</h1>
        <ul>
            @foreach($verbonden as $verbond)
                <li>
                    <a href="{{ route('verbonden.show', $verbond) }}">
                        Kazou {{ $verbond->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    </section>
@endsection
