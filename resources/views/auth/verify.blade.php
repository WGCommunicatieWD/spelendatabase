@extends('layouts.app')

@section('content')
    <section class="center">
        <h1>{{ __('Verify Your Email Address') }}</h1>

            @if (session('resent'))
                <div class="alert success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                    {{ __('This may take a while to arrive.') }}
                </div>
            @endif

            {{ __('Before proceeding, please check your email for a verification link.') }}
            {{ __('This may take a while to arrive.') }}
            {{ __('If you did not receive the email') }},
            <form action="{{ route('verification.resend') }}" method="post">
                @csrf
                <button type="submit">
                    <span class="fas fa-envelope"></span>
                    {{ __('click here to request another') }}
                </button>
            </form>
    </section>
@endsection
