@extends('layouts.app')
@section('title', __('Login'))

    @section('content')

        <section class="center">
            <h1>{{ __('Login') }}</h1>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <label for="email">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="{{ $errors->has('email') ? ' errorinput' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('email') }}
                    </div>
                @endif

                <label for="password">{{ __('Password') }}</label>
                <input id="password" type="password" class="{{ $errors->has('password') ? ' errorinput' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <div class="alert error" role="alert">
                        {{ $errors->first('password') }}
                    </div>
                @endif

                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">{{ __('Remember Me') }}</label>

                <div class="group">
                    <button type="submit" class="primary">
                        <span class="fa fa-user"></span>
                        {{ __('Login') }}
                    </button>
                </div>

            </form>
            <a href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        </section>
    @endsection

    @section('right-bar')
        <section class="center">
            <h1>Nog geen profiel?</h1>
            <p>
                Maak er hier één!
            </p>
            <a href="{{ route('register') }}" class="btn primary">
                <span class="fa fa-user-plus"></span>
                {{ __('Register') }}
            </a>
        </section>
    @endsection
