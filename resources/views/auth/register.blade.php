@extends('layouts.app')

@section('title', 'Registratie')

@section('content')
    <section class="center">

        <h1>{{ __('Register') }}</h1>

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <label for="name">{{ __('Name') }}</label>
            <input id="name" type="text" class="{{ $errors->has('name') ? ' errorinput' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <div class="alert error" role="alert">
                    {{ $errors->first('name') }}
                </div>
            @endif

            <label for="email">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="{{ $errors->has('email') ? ' errorinput' : '' }}" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <div class="alert error" role="alert">
                    {{ $errors->first('email') }}
                </div>
            @endif

            <label for="verbond_id">Verbond</label>
            <select id="verbond_id" name="verbond_id">
                @foreach (\App\Verbond::all() as $verbond)
                    <option value="{{ $verbond->id }}">{{ $verbond->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('verbond_id'))
                <div class="alert error" role="alert">
                    {{ $errors->first('verbond_id') }}
                </div>
            @endif

            <label for="password">{{ __('Password') }}</label>
            <input id="password" type="password" class="{{ $errors->has('password') ? ' errorinput' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <div class="alert error" role="alert">
                    {{ $errors->first('password') }}
                </div>
            @endif

            <label for="password-confirm">{{ __('Confirm Password') }}</label>
            <input id="password-confirm" type="password" name="password_confirmation" required>

            <div class="group">
                <button type="submit">
                    <span class="fa fa-user-plus"></span>
                    {{ __('Register') }}
                </button>
            </div>
        </form>

    </section>
@endsection
