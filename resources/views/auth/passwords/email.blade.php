@extends('layouts.app')

@section('content')
    <section class="center">
        <h1>{{ __('Reset Password') }}</h1>

        @if (session('status'))
            <div class="alert success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf
                <label for="email">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <div class="alert error" role="alert">
                    {{ $errors->first('email') }}
                </div>
            @endif

            <div class="center">
                <button type="submit">
                    <span class="fas fa-paper-plane"></span>
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
        </form>
    </section>
@endsection
