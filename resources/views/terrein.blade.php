@extends('layouts.app')

@section('title', $terrein->naam)

@section('content')
    <section class="center">
        <h1>{{ $terrein->naam }}</h1>
        @if ($spelen->count() < 1)
            <p>
                Er zijn geen spelen in <em>{{ $terrein->naam }}</em>
            </p>
        @endif
    </section>

    @component('components.shortspelen', ['spelen' => $spelen])
    @endcomponent

@endsection

@section('right-bar')
    <section class="center">
        <h3>Alle Categorieën</h3>
        <ul class="plain">
            @foreach ($terreinen as $ter)
                <li><a href="{{ route('terrein', $ter) }}">{{ $ter->naam }}</a></li>
            @endforeach
        </ul>
    </section>
    <section class="center">
        <h3>Alle Terreinen</h3>
        <ul class="plain">
            @foreach ($terreinen as $ter)
                <li><a href="{{ route('terrein', $ter) }}">{{ $ter->naam }}</a></li>
            @endforeach
        </ul>
    </section>
@endsection
