export function getCsrf() {
    return document.head.querySelector("[name~=csrf-token][content]").content;
}
