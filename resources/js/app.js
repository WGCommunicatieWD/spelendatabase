
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import last from 'lodash/last';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('base-modal', require('./components/BaseModal.vue').default);
Vue.component('add-user-to-group', require('./components/AddUserToGroup.vue').default);

//const files = require.context(
//    // Where to look
//    './components/',
//    // Recursive?
//    true,
//    // Regex to filter
//    /\.vue$/i);
//
//files.keys().map(key => {
//    return Vue.component(last(key.split('/')).split('.')[0], files(key));
//});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#main'
});
